﻿using System.Collections.Generic;

namespace SqlBulker.Core
{
    public struct Association
    {
        public string From { get; set; }
        public string Property { get; set; }

        #region Equality

        public bool Equals(Association other)
        {
            return string.Equals(From, other.From) && string.Equals(Property, other.Property);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Association && Equals((Association) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((From != null ? From.GetHashCode() : 0)*397) ^ (Property != null ? Property.GetHashCode() : 0);
            }
        }

        private sealed class FromPropertyEqualityComparer : IEqualityComparer<Association>
        {
            public bool Equals(Association x, Association y)
            {
                return string.Equals(x.From, y.From) && string.Equals(x.Property, y.Property);
            }

            public int GetHashCode(Association obj)
            {
                unchecked
                {
                    return ((obj.From != null ? obj.From.GetHashCode() : 0)*397) ^ (obj.Property != null ? obj.Property.GetHashCode() : 0);
                }
            }
        }

        private static readonly IEqualityComparer<Association> fromPropertyComparerInstance = new FromPropertyEqualityComparer();

        public static IEqualityComparer<Association> FromPropertyComparer
        {
            get { return fromPropertyComparerInstance; }
        }

        #endregion Equality
    }
}