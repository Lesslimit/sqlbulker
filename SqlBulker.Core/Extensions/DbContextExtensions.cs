﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SqlBulker.Core.Extensions
{
    public static class DbContextExtensions
    {
        public static string GetTableName<TEntity>(this DbContext dbContext)
        {
            return dbContext.GetTableName(typeof (TEntity));
        }

        public static string GetTableName(this DbContext dbContext, Type entityType)
        {
            return GetEntitySet(dbContext, entityType).ToFullTableName();
        }

        public static string ToFullTableName(this EntitySetBase entitySet)
        {
            return String.Format("[{0}].[{1}]", entitySet.Schema, entitySet.Table);
        }

        public static ObjectContext GetObjectContext(this DbContext dbContext)
        {
            return ((IObjectContextAdapter)dbContext).ObjectContext;
        }

        public static SqlDbType ToSqlDbType(this EdmProperty edmProperty)
        {
            string typeName = edmProperty.TypeName.Replace("(max)", string.Empty);

            SqlDbType sqlDbType;
            if (Enum.TryParse(typeName, true, out sqlDbType))
            {
                return sqlDbType;
            }

            throw new ArgumentException("", "edmProperty");
        }

        public static void SaveChangesBulk(this DbContext dbContext)
        {
            dbContext.SaveChangesBulkAsync().Wait();
        }

        public static async Task SaveChangesBulkAsync(this DbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (!dbContext.ChangeTracker.HasChanges())
            {
                return;
            }

            DbEntityEntry[] dbEntityEntries = dbContext.ChangeTracker.Entries().ToArray();

            var sqlConnection = (SqlConnection)dbContext.Database.Connection;
            if (sqlConnection.State == ConnectionState.Closed)
            {
                sqlConnection.Open();
            }

            SqlTransaction transaction = null;
            DataTable previouseDataSource;
            try
            {
                transaction = sqlConnection.BeginTransaction(IsolationLevel.ReadCommitted);

                foreach (var entitiesGroup in dbEntityEntries.GroupBy(dee => GetObjectType(dee.Entity.GetType())))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        transaction.Rollback();
                        transaction.Dispose();

                        return;
                    }

                    Type entityType = entitiesGroup.Key;
                    string tableName = dbContext.GetTableName(entityType);

                    var table = new DataTable(tableName);
                    var sqlBulkCopy = new SqlBulkCopy(sqlConnection,
                                                      SqlBulkCopyOptions.KeepNulls | SqlBulkCopyOptions.KeepIdentity |
                                                      SqlBulkCopyOptions.FireTriggers, transaction)
                    {
                        DestinationTableName = tableName,
                        BatchSize = entitiesGroup.Count()
                    };

                    PropertyDescriptor[] props = GetSimpleProperties(entityType);

                    foreach (var propertyInfo in props)
                    {
                        sqlBulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);
                        table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                    }

                    foreach (DbEntityEntry entityEntry in entitiesGroup)
                    {
                        var values = new object[props.Length];
                        for (var i = 0; i < values.Length; i++)
                        {
                            values[i] = entityEntry.CurrentValues[props[i].Name];
                        }

                        table.Rows.Add(values);
                    }

                    var sqlBulkOperationSetting = SqlBulkOperationSetting.InitializeFrom(dbContext.GetObjectContext(), entityType);
                    previouseDataSource = await sqlBulkCopy.BulkMergeAsync(table, sqlBulkOperationSetting, cancellationToken);
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                throw;
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        #region Private Methods

        private static PropertyDescriptor[] GetSimpleProperties(Type entityType)
        {
            return TypeDescriptor.GetProperties(entityType)
                                 .Cast<PropertyDescriptor>()
                                 .Where(propertyInfo => propertyInfo.PropertyType.Namespace != null
                                                        && propertyInfo.PropertyType.Namespace.Equals("System")
                                                        && !propertyInfo.Attributes.OfType<NotMappedAttribute>().Any()
                                                        || propertyInfo.PropertyType.IsEnum)
                                 .ToArray();
        }

        private static EntitySetBase GetEntitySet(IObjectContextAdapter dbContext, Type type)
        {
            type = GetObjectType(type);
            EntitySetBase es = GetEntitySetByName(dbContext, type.Name, type.BaseType != null ? type.BaseType.Name : null);

            if (es == null)
            {
                throw new ArgumentException("Entity type not found in GetEntitySet", type.Name);
            }

            return es;
        }

        private static EntitySetBase GetEntitySetByName(IObjectContextAdapter dbContext, string typeName, string baseTypeName = null)
        {
            return dbContext.ObjectContext.MetadataWorkspace
                                          .GetItemCollection(DataSpace.SSpace)
                                          .GetItems<EntityContainer>()
                                          .SelectMany(c => c.BaseEntitySets
                                                            .Where(e => e.Name == typeName || e.Name == baseTypeName))
                                          .FirstOrDefault();
        }

        private static Type GetObjectType(Type type)
        {
            return ObjectContext.GetObjectType(type);
        }

        #endregion Private Methods
    }
}