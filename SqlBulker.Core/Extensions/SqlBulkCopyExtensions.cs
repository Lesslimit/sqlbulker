﻿using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace SqlBulker.Core.Extensions
{
    public static class SqlBulkCopyExtensions
    {
        public static DataTable BulkMerge(this SqlBulkCopy sqlBulkCopy, DataTable dataTable, SqlBulkOperationSetting setting)
        {
            if (dataTable.Rows.Count > 0)
            {
                var bulkOperation = new SqlBulkOperation(new SqlBulkOperationContext(setting)
                {
                    SqlBulkCopy = sqlBulkCopy,
                    DataSource = dataTable,
                    Settings = setting
                });

                return bulkOperation.Execute();
            }

            return null;
        }

        public static async Task<DataTable> BulkMergeAsync(this SqlBulkCopy sqlBulkCopy, DataTable dataTable, 
                                                     SqlBulkOperationSetting setting, CancellationToken cancellationToken)
        {
            if (dataTable.Rows.Count > 0)
            {
                var bulkOperation = new SqlBulkOperation(new SqlBulkOperationContext(setting)
                {
                    SqlBulkCopy = sqlBulkCopy,
                    DataSource = dataTable,
                    Settings = setting
                });

                return await bulkOperation.ExecuteAsync(cancellationToken);
            }

            return null;
        }
    }
}