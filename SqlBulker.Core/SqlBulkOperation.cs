﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SqlBulker.Core
{
    public class SqlBulkOperation
    {
        #region Fields

        private readonly SqlBulkOperationContext operationContext;
        private static readonly ConcurrentDictionary<string, Queue<object>> insertedIds = new ConcurrentDictionary<string, Queue<object>>();

        private DataTable internalDataSource;

        private List<SqlBulkCopyColumnMapping> mappingList;

        private string sqlActionBulkUpdate;

        private string sqlActionBulkInsert;

        private string sqlActionDropTemporaryTable;

        private SqlConnection sqlConnection;

        private SqlTransaction sqlTransaction;

        private string temporaryColumnName;

        private string temporaryTableName;

        #endregion Fields

        #region Properties

        private SqlBulkOperationSetting Settings
        {
            get { return operationContext.Settings; }
        }

        private bool IsValid
        {
            get
            {
                if (Settings.PrimaryKeys.Count == 0 && string.IsNullOrEmpty(Settings.IdentityColumn))
                {
                    throw new Exception(
                        "Primary keys column name cannot be empty, at least only column must be used as primary key.");
                }
                if (Settings.PrimaryKeys.Exists(x => !mappingList.Exists(y => y.DestinationColumn == x)))
                {
                    throw new Exception("All primary key column name must be mapped.");
                }

                if (SqlBulkCopy.ColumnMappings.Count == 0)
                {
                    throw new Exception("Column mapping must be specified for Bulk operation.");
                }

                if (!(DataSource is DataTable || DataSource is IEnumerable<object>))
                {
                    throw new Exception("Invalid datasource");
                }

                return true;
            }
        }

        private bool IsBulkRequired
        {
            get
            {
                if (operationContext.Config.MinRecord > 0)
                {
                    var table = DataSource as DataTable;
                    if (table != null)
                    {
                        return table.Rows.Count >= operationContext.Config.MinRecord;
                    }
                }

                return true;
            }
        }

        private bool HasRow
        {
            get
            {
                var table = DataSource as DataTable;
                return table == null || table.Rows.Count != 0;
            }
        }

        private SqlBulkCopy SqlBulkCopy
        {
            get { return operationContext.SqlBulkCopy; }
        }

        private object DataSource
        {
            get { return operationContext.DataSource; }
        }

        #endregion Properties

        public SqlBulkOperation(SqlBulkOperationContext operationContext)
        {
            this.operationContext = operationContext;
        }

        public DataTable Execute()
        {
            ExecuteAsync(CancellationToken.None).Wait();

            return internalDataSource;
        }

        public async Task<DataTable> ExecuteAsync(CancellationToken cancellationToken)
        {
            PreLogicSetting();

            if (!HasRow)
            {
                return null;
            }

            if (IsBulkRequired)
            {
                await SetCommonSettingAsync(cancellationToken).ConfigureAwait(false);

                EnsureTransactionIsSet();

                if (!IsValid)
                {
                    return null;
                }

                SetInternalDataSourceBulk();
                AddCustomSetting();

                SetSqlMergeActionBulk();
                SetSqlDropTemporaryTable();

                try
                {
                    await CreateTemporaryTableAsync(cancellationToken).ConfigureAwait(false);
                    await WriteToServerAsync(cancellationToken).ConfigureAwait(false);
                    await ExecuteBulkAsync(cancellationToken).ConfigureAwait(false);
                    await DropTemporaryTableAsync(cancellationToken).ConfigureAwait(false);
                }
                finally
                {
                    RemoveCustomSetting();
                }
            }
            else
            {
                await SetCommonSettingAsync(cancellationToken).ConfigureAwait(false);

                if (!IsValid)
                {
                    return null;
                }

                SetInternalDataSourceSingle();
                SetSqlMergeActionSingle();
                await ExecuteNonBulkAsync(cancellationToken).ConfigureAwait(false);
            }

            return internalDataSource;
        }

        private void PreLogicSetting()
        {
            operationContext.ParameterMapping = new Dictionary<string, int>();
            for (int i = 0; i < SqlBulkCopy.ColumnMappings.Count; i++)
            {
                operationContext.ParameterMapping.Add(SqlBulkCopy.ColumnMappings[i].DestinationColumn, i);
            }
        }

        private void EnsureTransactionIsSet()
        {
            if (sqlTransaction == null)
            {
                sqlTransaction = sqlConnection.BeginTransaction();
                // ReSharper disable PossibleNullReferenceException
                SqlBulkCopy.GetType().GetField("_externalTransaction", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(SqlBulkCopy, sqlTransaction);
                // ReSharper restore PossibleNullReferenceException
            }
        }

        private void SetInternalDataSourceBulk()
        {
            bool isDataTable = DataSource is DataTable;

            if (SqlBulkCopy.BatchSize == 0)
            {
                if (isDataTable)
                {
                    internalDataSource = DataSource as DataTable;
                }
            }
            else
            {
                if (isDataTable)
                {
                    internalDataSource = (DataSource as DataTable).Copy();
                }

                // Add a fake "identity" column to use in the bulk action.
                var dtSource = (DataTable)internalDataSource;
                dtSource.Columns.Add(temporaryColumnName);

                Association association = Settings.Associations.FirstOrDefault(a => insertedIds.Keys.Contains(a.From));
                object[] insertedAssociationIds = null;
                if (!association.Equals(default(Association)))
                {
                    Queue<object> insertedId = insertedIds[association.From];
                    var i = (dtSource.Rows.Count / insertedId.Count) - 1;

                    while (i > 0)
                    {
                        insertedId = new Queue<object>(insertedId.Concat(insertedId));
                        i--;
                    }

                    insertedAssociationIds = insertedId.ToArray();
                }

                for (int i = 0; i < dtSource.Rows.Count; i++)
                {
                    DataRow row = dtSource.Rows[i];
                    if (insertedAssociationIds != null)
                    {
                        row[association.Property] = insertedAssociationIds[i];
                    }

                    row[temporaryColumnName] = i + 1;
                }

                internalDataSource = dtSource;
            }
        }

        private void AddCustomSetting()
        {
            if (SqlBulkCopy.BatchSize > 0)
            {
                SqlBulkCopy.ColumnMappings.Add(temporaryColumnName, temporaryColumnName);

                mappingList = SqlBulkCopy.ColumnMappings.Cast<SqlBulkCopyColumnMapping>().ToList();
            }
        }

        private void SetSqlMergeActionBulk()
        {
            Func<SqlBulkCopyColumnMapping, bool> columnsToSetFilter;
            Func<SqlBulkCopyColumnMapping, bool> columnsToInsertFilter;

            if (!Settings.InsertIdentity)
            {
                columnsToSetFilter = x => !Settings.PrimaryKeys.Contains(x.DestinationColumn)
                                          && x.DestinationColumn != Settings.IdentityColumn
                                          && x.DestinationColumn != temporaryColumnName;

                columnsToInsertFilter = x => x.DestinationColumn != Settings.IdentityColumn
                                             && x.DestinationColumn != temporaryColumnName;
            }
            else
            {
                columnsToSetFilter = columnsToInsertFilter = x => x.DestinationColumn != temporaryColumnName;
            }

            IEnumerable<string> sqlWhereSelectList = Settings.PrimaryKeys.Select(x => string.Format("A.[{0}] = B.[{0}]", x));

            IEnumerable<string> sqlSetValueSelectList = mappingList.Where(columnsToSetFilter)
                                                                   .Select(x => string.Format("A.[{0}] = B.[{0}]", x.DestinationColumn));

            IEnumerable<string> sqlInsertColumnSelectList = mappingList.Where(columnsToInsertFilter)
                                                                       .Select(x => string.Format("[{0}]", x.DestinationColumn));

            string sqlSetValue = string.Join(", ", sqlSetValueSelectList.ToArray());
            string sqlWhere = String.Join(" AND ", sqlWhereSelectList.ToArray());
            string sqlInserColumns = string.Join(", ", sqlInsertColumnSelectList.ToArray());

            string sqlActionUpdate = string.Format(SqlBulkOperationConstants.SqlUpdateActionBulk, SqlBulkCopy.DestinationTableName, temporaryTableName, sqlSetValue, sqlWhere);
            string sqlActionInsert = string.Format(SqlBulkOperationConstants.SqlMergeInsertActionBulk, SqlBulkCopy.DestinationTableName, temporaryTableName, sqlWhere, sqlInserColumns, sqlInserColumns);

            sqlActionBulkUpdate = (SqlBulkCopy.BatchSize == 0
                ? sqlActionUpdate
                : string.Format(SqlBulkOperationConstants.SqlBatchOperation, temporaryTableName, sqlActionUpdate, temporaryColumnName, SqlBulkCopy.BatchSize));

            sqlActionBulkInsert = (SqlBulkCopy.BatchSize == 0
                ? sqlActionInsert
                : string.Format(SqlBulkOperationConstants.SqlBatchOperationAnd, temporaryTableName, sqlActionInsert, temporaryColumnName, SqlBulkCopy.BatchSize));
        }

        private void SetSqlDropTemporaryTable()
        {
            sqlActionDropTemporaryTable = string.Format(SqlBulkOperationConstants.SqlDropTable, temporaryTableName);
        }

        private async Task CreateTemporaryTableAsync(CancellationToken cancellationToken)
        {
            string tableName = temporaryTableName;
            List<string> list = Settings.TableColumns;
            var isSqlAzure = await Settings.IsSqlAzureAsync(sqlConnection, sqlTransaction, cancellationToken);

            var sb = new StringBuilder();
            sb.AppendLine("CREATE TABLE " + tableName);
            sb.AppendLine("(");

            if (isSqlAzure)
            {
                // TODO: Remove identity for performance issue?
                sb.AppendLine("[8FEF2341_951C_47B6_BF7E_D350E54FFE4D] INT IDENTITY(1, 1), ");
            }

            sb.AppendLine(string.Join("," + Environment.NewLine, list.ToArray()));

            // Add default value to custom column
            if (SqlBulkCopy.BatchSize > 0)
            {
                sb.AppendLine(string.Concat(", ", "[", temporaryColumnName, "] INT NULL"));
            }

            sb.AppendLine(isSqlAzure
                ? " CONSTRAINT [PK_8FEF2341_951C_47B6_BF7E_D350E54FFE4D] PRIMARY KEY CLUSTERED ( [8FEF2341_951C_47B6_BF7E_D350E54FFE4D] ASC) )"
                : ")");

            await CreateTemporaryTableAsync(sb.ToString(), cancellationToken);
        }

        private async Task CreateTemporaryTableAsync(string sql, CancellationToken cancellationToken)
        {
            using (var sqlCommand = new SqlCommand(sql, sqlConnection, sqlTransaction))
            {
                await sqlCommand.ExecuteNonQueryAsync(cancellationToken);
            }
        }

        private async Task WriteToServerAsync(CancellationToken cancellationToken, bool useTemporaryTable = true)
        {
            string originalDestinationName = SqlBulkCopy.DestinationTableName;

            // Only bulk insert doesn't use temporary table. 
            if (useTemporaryTable)
            {
                SqlBulkCopy.DestinationTableName = string.Concat("[", temporaryTableName + "]");
            }

            await SqlBulkCopy.WriteToServerAsync(internalDataSource, cancellationToken);

            if (useTemporaryTable)
            {
                SqlBulkCopy.DestinationTableName = originalDestinationName;
            }
        }

        private async Task ExecuteBulkAsync(CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(sqlActionBulkUpdate))
            {
                using (var sqlCommand = new SqlCommand(sqlActionBulkUpdate, sqlConnection))
                {
                    sqlCommand.Transaction = sqlTransaction;
                    await sqlCommand.ExecuteNonQueryAsync(cancellationToken);
                }
            }

            if (!string.IsNullOrEmpty(sqlActionBulkInsert))
            {
                using (var sqlCommand = new SqlCommand(sqlActionBulkInsert, sqlConnection))
                {
                    sqlCommand.Transaction = sqlTransaction;
                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync(CommandBehavior.Default, cancellationToken);
                    int i = 0;

                    while (await sqlDataReader.ReadAsync(cancellationToken))
                    {
                        object newId = sqlDataReader.GetValue(0);
                        insertedIds.AddOrUpdate(SqlBulkCopy.DestinationTableName, s =>
                        {
                            Queue<object> queue;
                            if (insertedIds.TryGetValue(SqlBulkCopy.DestinationTableName, out queue))
                            {
                                queue.Enqueue(newId);
                            }
                            
                            queue = new Queue<object>();
                            queue.Enqueue(newId);
                            internalDataSource.Rows[i][Settings.IdentityColumn] = newId;

                            i++;
                            return queue;
                        }, (s, queue) =>
                        {
                            insertedIds[s].Enqueue(newId);
                            internalDataSource.Rows[i][Settings.IdentityColumn] = newId;

                            i++;
                            return insertedIds[s];
                        });
                    }
                }
            }
        }

        private async Task DropTemporaryTableAsync(CancellationToken cancellationToken)
        {
            using (var sqlCommand = new SqlCommand(sqlActionDropTemporaryTable, sqlConnection))
            {
                sqlCommand.Transaction = sqlTransaction;
                await sqlCommand.ExecuteNonQueryAsync(cancellationToken);
            }
        }

        private async Task SetCommonSettingAsync(CancellationToken cancellationToken)
        {
            mappingList = SqlBulkCopy.ColumnMappings.Cast<SqlBulkCopyColumnMapping>().ToList();

            // ReSharper disable PossibleNullReferenceException
            Type type = SqlBulkCopy.GetType();
            sqlConnection = type.GetField("_connection", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(SqlBulkCopy) as SqlConnection;
            sqlTransaction = type.GetField("_externalTransaction", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(SqlBulkCopy) as SqlTransaction;
            // ReSharper restore PossibleNullReferenceException

            if (IsBulkRequired)
            {
                string prefix = await Settings.IsSqlAzureAsync(sqlConnection, sqlTransaction, cancellationToken) ? "ZZZ_" : "#";
                temporaryTableName = string.Concat(prefix, Guid.NewGuid().ToString().Replace("-", "_"));
                temporaryColumnName = Guid.NewGuid().ToString().Replace("-", "_");
            }
        }

        private void SetInternalDataSourceSingle()
        {
            if (DataSource is DataTable)
            {
                internalDataSource = DataSource as DataTable;
            }
        }

        private void SetSqlMergeActionSingle()
        {
            string sqlSetValue;
            string sqlWhere;
            string sqlInsertColumn;
            string sqlInsertValue;

            {
                IEnumerable<string> updateColumnSelectList = mappingList.Where(x => !Settings.PrimaryKeys.Contains(x.DestinationColumn)
                                                                                    && x.DestinationColumn != Settings.IdentityColumn
                                                                                    && x.DestinationColumn != temporaryColumnName)
                                                                        .Select(x => string.Format("[{0}]" + "=" + "@{1}", x.DestinationColumn, operationContext.ParameterMapping[x.DestinationColumn]));

                IEnumerable<string> sqlWhereSelectList = Settings.PrimaryKeys.Select(x => string.Format("[{0}]" + "=" + "@{1}", x, operationContext.ParameterMapping[x]));

                sqlSetValue = string.Join(", ", updateColumnSelectList.ToArray());
                sqlWhere = String.Join(" AND ", sqlWhereSelectList.ToArray());
            }

            {
                IEnumerable<string> updateColumnSelectList = mappingList.Where(x => x.DestinationColumn != Settings.IdentityColumn)
                                                                        .Select(x => string.Format("[{0}]", x.DestinationColumn));

                IEnumerable<string> updateColumnInsertList = mappingList.Where(x => x.DestinationColumn != Settings.IdentityColumn)
                                                                        .Select(x => string.Format("@{0}", operationContext.ParameterMapping[x.DestinationColumn]));

                sqlInsertColumn = string.Join(", ", updateColumnSelectList.ToArray());
                sqlInsertValue = string.Join(", ", updateColumnInsertList.ToArray());
            }

            sqlActionBulkUpdate = string.Format(SqlBulkOperationConstants.SqlMergeActionSingle, SqlBulkCopy.DestinationTableName, null, sqlSetValue, sqlWhere, sqlInsertColumn, sqlInsertValue)
                                              + SqlBulkOperationConstants.SqlSelectScopeIdentity;
        }

        private async Task ExecuteNonBulkAsync(CancellationToken cancellationToken)
        {
            var table = internalDataSource as DataTable;
            if (table != null)
            {
                var list = table;
                foreach (DataRow dr in list.Rows)
                {
                    using (var sqlCommand = new SqlCommand(sqlActionBulkUpdate, sqlConnection, sqlTransaction))
                    {
                        Association association = Settings.Associations.FirstOrDefault(a => insertedIds.Keys.Contains(a.From));
                        Queue<object> insertedAssociationIds = null;
                        if (!association.Equals(default(Association)))
                        {
                            insertedAssociationIds = new Queue<object>(insertedIds[association.From]);
                        }

                        foreach (SqlBulkCopyColumnMapping param in mappingList)
                        {
                            if (insertedAssociationIds != null && association.Property == param.DestinationColumn)
                            {
                                dr[param.SourceColumn] = insertedAssociationIds.Dequeue();
                            }

                            sqlCommand.Parameters.Add(new SqlParameter
                            {
                                ParameterName = "@" + operationContext.ParameterMapping[param.DestinationColumn],
                                SqlDbType = Settings.MappingColumnWithDbType[param.DestinationColumn],
                                Value = dr[param.SourceColumn]
                            });
                        }

                        sqlCommand.Transaction = sqlTransaction;
                        object newId = await sqlCommand.ExecuteScalarAsync(cancellationToken);

                        insertedIds.AddOrUpdate(SqlBulkCopy.DestinationTableName, s =>
                        {
                            Queue<object> queue;
                            if (insertedIds.TryGetValue(SqlBulkCopy.DestinationTableName, out queue))
                            {
                                queue.Enqueue(newId);
                            }
                            queue = new Queue<object>();
                            queue.Enqueue(newId);

                            return queue;
                        }, (s, queue) =>
                        {
                            insertedIds[s].Enqueue(newId);
                            return insertedIds[s];
                        });
                    }
                }
            }
        }

        private void RemoveCustomSetting()
        {
            if (SqlBulkCopy.BatchSize > 0)
            {
                SqlBulkCopy.ColumnMappings.RemoveAt(SqlBulkCopy.ColumnMappings.Count - 1);
            }
        }
    }
}