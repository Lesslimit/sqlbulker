﻿namespace SqlBulker.Core
{
    public class SqlBulkOperationConfig
    {
        public int MinRecord { get; set; }

        public SqlBulkOperationConfig()
        {
            MinRecord = 11;
        }
    }
}