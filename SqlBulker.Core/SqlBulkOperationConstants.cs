﻿namespace SqlBulker.Core
{
    public class SqlBulkOperationConstants
    {
        /// <summary>The SQL Batch operation with where clause.</summary>
        internal const string SqlBatchOperation = @"
DECLARE @BatchSize INT
DECLARE @NbItem INT
DECLARE @I INT

SET @BatchSize = {3}
SET @NbItem = (SELECT COUNT(1) FROM {0})
SET @I = 1

WHILE @I <= @NbItem
BEGIN
    {1}
    WHERE [{2}] >= @I AND [{2}] < @I + @BatchSize

    SET @I = @I + @BatchSize
END
";

        internal const string SqlUpdateActionBulk = "UPDATE A SET {2} FROM {0} AS A INNER JOIN {1} AS B ON {3}";

        internal const string SqlMergeInsertActionBulk =
            "INSERT INTO {0} ({3}) OUTPUT Inserted.ID into @IDs SELECT {4} FROM {1} AS B WHERE NOT EXISTS (SELECT 1 FROM {0} AS A WHERE {2})";

        /// <summary>The SQL Batch operation with and clause.</summary>
        internal const string SqlBatchOperationAnd = @"
DECLARE @BatchSize INT;
DECLARE @NbItem INT;
DECLARE @I INT;
DECLARE @IDs TABLE(ID INT);

SET @BatchSize = {3}
SET @NbItem = (SELECT COUNT(1) FROM {0})
SET @I = 1

WHILE @I <= @NbItem
BEGIN
    {1}
    AND [{2}] >= @I AND [{2}] < @I + @BatchSize

    SET @I = @I + @BatchSize
    --INSERT INTO @IDs VALUES(SCOPE_IDENTITY());
END

SELECT ID FROM @IDs;
";

        internal const string SqlDropTable = "DROP TABLE {0}";

        internal const string SqlMergeActionSingle =
            "UPDATE {0} SET {2} WHERE {3} INSERT INTO {0} ({4}) SELECT {5} WHERE NOT EXISTS (SELECT 1 FROM {0} WHERE {3})";

        internal const string SqlSelectScopeIdentity = "; SELECT SCOPE_IDENTITY();";
    }
}