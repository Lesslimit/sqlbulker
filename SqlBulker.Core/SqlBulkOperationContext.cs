﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace SqlBulker.Core
{
    public class SqlBulkOperationContext
    {
        public Dictionary<string, int> ParameterMapping { get; set; }

        public SqlBulkOperationSetting Settings { get; set; }

        public SqlBulkOperationConfig Config { get; set; }

        public SqlBulkCopy SqlBulkCopy { get; set; }

        public object DataSource { get; set; }

        public SqlBulkOperationContext(SqlBulkOperationSetting settings, SqlBulkOperationConfig config = null)
        {
            Settings = settings;
            Config = config ?? new SqlBulkOperationConfig();
        }
    }
}