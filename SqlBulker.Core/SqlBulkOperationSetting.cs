﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SqlBulker.Core.Extensions;

namespace SqlBulker.Core
{
    public class SqlBulkOperationSetting
    {
        private static bool? isAzure;

        public bool InsertIdentity { get; set; }

        public string IdentityColumn { get; set; }

        public Dictionary<string, SqlDbType> MappingColumnWithDbType { get; set; }

        public List<string> TableColumns { get; set; }

        public List<string> PrimaryKeys { get; set; }

        public IList<Association> Associations { get; set; }

        public SqlBulkOperationSetting()
        {
            TableColumns = new List<string>();
            PrimaryKeys = new List<string>();
            Associations = new List<Association>();
            MappingColumnWithDbType = new Dictionary<string, SqlDbType>();
        }

        public async Task<bool> IsSqlAzureAsync(SqlConnection connection, SqlTransaction transaction,
                                                CancellationToken cancellationToken = default(CancellationToken))
        {
            if (isAzure.HasValue)
            {
                return isAzure.Value;
            }

            using (var command = new SqlCommand("SELECT CAST(SERVERPROPERTY('EngineEdition') AS INT)", connection, transaction))
            {
                using (var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleResult, cancellationToken))
                {
                    reader.Read();

                    const int sqlAzureEngineEdition = 5;
                    return (isAzure = reader.GetInt32(0) == sqlAzureEngineEdition).Value;
                }
            }
        }

        public static SqlBulkOperationSetting InitializeFrom(ObjectContext objectContext, Type type)
        {
            var settings = new SqlBulkOperationSetting();

            type = ObjectContext.GetObjectType(type);
            string typeName = type.Name;

            EntityType entityType = objectContext.MetadataWorkspace
                                                 .GetItemCollection(DataSpace.SSpace)
                                                 .AsParallel()
                                                 .OfType<EntityType>()
                                                 .FirstOrDefault(et => et.Name == typeName);

            AssociationType[] associationsTypes = objectContext.MetadataWorkspace
                                                               .GetItemCollection(DataSpace.CSpace)
                                                               .AsParallel()
                                                               .OfType<AssociationType>()
                                                               .Where(at => at.IsForeignKey
                                                                        && (at.KeyMembers.Any(em => ((RefType)em.TypeUsage.EdmType).ElementType.Name == typeName)))
                                                               .ToArray();

            foreach (var associationType in associationsTypes)
            {
                ReferentialConstraint constraint = associationType.ReferentialConstraints[0];
                EntityType fromRole = GetEntityTypeForEndMember(constraint.FromRole);

                foreach (var toProperty in constraint.ToProperties)
                {
                    settings.Associations.Add(new Association
                    {
                        From = GetEntitySetByName(objectContext, fromRole.Name).ToFullTableName(),
                        Property = toProperty.Name
                    });
                }
            }

            if (entityType != null)
            {
                EdmMember primaryKey = entityType.KeyMembers.First();
                settings.IdentityColumn = primaryKey.Name;
                settings.PrimaryKeys.Add(primaryKey.Name);
                settings.InsertIdentity = !primaryKey.IsStoreGeneratedIdentity;

                foreach (var property in entityType.Properties)
                {
                    bool isMax = property.TypeName.EndsWith("(max)", StringComparison.OrdinalIgnoreCase);

                    SqlDbType sqlDbType = property.ToSqlDbType();
                    string sqlInfo = string.Format("[{0}] {1}", property.Name, sqlDbType);

                    if (property.IsFixedLengthConstant)
                    {
                        sqlInfo += "(" + (isMax ? 4000 : property.MaxLength) + ")";
                    }

                    settings.TableColumns.Add(sqlInfo + (property.Nullable ? " NULL" : string.Empty));
                    settings.MappingColumnWithDbType.Add(property.Name, sqlDbType);
                }
            }

            return settings;
        }

        private static EntityType GetEntityTypeForEndMember(EdmMember end)
        {
            var refType = (RefType)end.TypeUsage.EdmType;
            EntityTypeBase endType = refType.ElementType;

            return (EntityType)endType;
        }

        private static EntitySetBase GetEntitySetByName(ObjectContext objectContext, string typeName, string baseTypeName = null)
        {
            return objectContext.MetadataWorkspace
                                .GetItemCollection(DataSpace.SSpace)
                                .GetItems<EntityContainer>()
                                .SelectMany(c => c.BaseEntitySets
                                                  .Where(e => e.Name == typeName || e.Name == baseTypeName))
                                .FirstOrDefault();
        }
    }
}