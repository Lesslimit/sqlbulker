namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountManager")]
    public partial class AccountManager
    {
        public AccountManager()
        {
            AgencyProfiles = new HashSet<AgencyProfile>();
        }

        public Guid AccountManagerID { get; set; }

        [Required]
        [StringLength(250)]
        public string AccountManagerFullName { get; set; }

        public virtual ICollection<AgencyProfile> AgencyProfiles { get; set; }
    }
}
