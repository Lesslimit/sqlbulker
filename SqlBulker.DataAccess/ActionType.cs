﻿namespace SqlBulker.DataAccess
{
    public enum ActionType
    {
        Created = 0,
        Edited = 1,
        Notified = 2,
        Locked = 3,
        Deleted = 4,
        Posted = 5
    }
}