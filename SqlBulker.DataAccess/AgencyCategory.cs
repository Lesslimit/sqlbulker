namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgencyCategory")]
    public partial class AgencyCategory
    {
        public AgencyCategory()
        {
            CompanyCategories = new HashSet<CompanyCategory>();
        }

        public int Id { get; set; }

        public Guid AgencyId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public int DisplayOrder { get; set; }

        public int Status { get; set; }

        public virtual AgencyProfile AgencyProfile { get; set; }

        public virtual ICollection<CompanyCategory> CompanyCategories { get; set; }
    }
}
