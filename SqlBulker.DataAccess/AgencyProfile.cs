namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgencyProfile")]
    public partial class AgencyProfile
    {
        public AgencyProfile()
        {
            AgencyCategories = new HashSet<AgencyCategory>();
            AgencyRefreshes = new HashSet<AgencyRefresh>();
            AgencyReports = new HashSet<AgencyReport>();
            UserFolders = new HashSet<UserFolder>();
        }

        [Key]
        public Guid AgencyId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public Guid? PartnerId { get; set; }

        public int Type { get; set; }

        public int FreeLocationsCount { get; set; }

        public DateTime? DateCreated { get; set; }

        public int Status { get; set; }

        public Guid ScoreRuleSetId { get; set; }

        public Guid? AccountManagerID { get; set; }

        public int? PartialRefreshFrequency { get; set; }

        public int? FullRefreshFrequency { get; set; }

        public int? SubscriberStatus { get; set; }

        public int? ReviewRefreshFrequency { get; set; }

        public DateTime? NextFullRefreshDate { get; set; }

        public DateTime? NextPartialRefreshDate { get; set; }

        public DateTime? NextReviewsRefreshDate { get; set; }

        public virtual AccountManager AccountManager { get; set; }

        public virtual ICollection<AgencyCategory> AgencyCategories { get; set; }

        public virtual ScoreRuleSet ScoreRuleSet { get; set; }

        public virtual ICollection<AgencyRefresh> AgencyRefreshes { get; set; }

        public virtual ICollection<AgencyReport> AgencyReports { get; set; }

        public virtual AgencySetting AgencySetting { get; set; }

        public virtual ICollection<UserFolder> UserFolders { get; set; }
    }
}
