namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgencyRefresh")]
    public partial class AgencyRefresh
    {
        public Guid Id { get; set; }

        public Guid AgencyId { get; set; }

        public int Type { get; set; }

        public int? BuildType { get; set; }

        public int? RequestSource { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid? FootprintBuildsSetId { get; set; }

        public int Status { get; set; }

        public Guid? RequestorId { get; set; }

        public DateTime? DateFinished { get; set; }

        public virtual AgencyProfile AgencyProfile { get; set; }

        public virtual FootprintBuildsSet FootprintBuildsSet { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
