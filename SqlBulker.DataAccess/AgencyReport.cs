namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgencyReport")]
    public partial class AgencyReport
    {
        public AgencyReport()
        {
            GeneratedAgencyReports = new HashSet<GeneratedAgencyReport>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid AgencyId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid ReportId { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual AgencyProfile AgencyProfile { get; set; }

        public virtual Report Report { get; set; }

        public virtual ICollection<GeneratedAgencyReport> GeneratedAgencyReports { get; set; }
    }
}
