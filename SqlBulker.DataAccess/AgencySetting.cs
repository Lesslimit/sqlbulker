namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AgencySetting
    {
        [Key]
        public Guid AgencyId { get; set; }

        public int? ThresholdId { get; set; }

        public bool ReviewResponseEnabled { get; set; }

        public int? ReviewResponseDaysRequired { get; set; }

        public virtual AgencyProfile AgencyProfile { get; set; }

        public virtual Threshold Threshold { get; set; }
    }
}
