namespace SqlBulker.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BrandifyEntities : DbContext
    {
        public BrandifyEntities()
            : base("name=BrandifyEntities")
        {
        }

        public virtual DbSet<AccountManager> AccountManagers { get; set; }
        public virtual DbSet<AgencyCategory> AgencyCategories { get; set; }
        public virtual DbSet<AgencyProfile> AgencyProfiles { get; set; }
        public virtual DbSet<AgencyRefresh> AgencyRefreshes { get; set; }
        public virtual DbSet<AgencyReport> AgencyReports { get; set; }
        public virtual DbSet<AgencySetting> AgencySettings { get; set; }
        public virtual DbSet<BuildType> BuildTypes { get; set; }
        public virtual DbSet<BusinessCenter> BusinessCenters { get; set; }
        public virtual DbSet<CategorySEOPosition> CategorySEOPositions { get; set; }
        public virtual DbSet<Community> Communities { get; set; }
        public virtual DbSet<CommunitySubscriber> CommunitySubscribers { get; set; }
        public virtual DbSet<CommunityTopic> CommunityTopics { get; set; }
        public virtual DbSet<CommunityTopicFollower> CommunityTopicFollowers { get; set; }
        public virtual DbSet<CompanyAddress> CompanyAddresses { get; set; }
        public virtual DbSet<CompanyBusinessType> CompanyBusinessTypes { get; set; }
        public virtual DbSet<CompanyCategory> CompanyCategories { get; set; }
        public virtual DbSet<CompanyCompetitor> CompanyCompetitors { get; set; }
        public virtual DbSet<CompanyDataQualityInfo> CompanyDataQualityInfoes { get; set; }
        public virtual DbSet<CompanyDataQualityWarningCodesInfo> CompanyDataQualityWarningCodesInfoes { get; set; }
        public virtual DbSet<CompanyHistoricalInfo> CompanyHistoricalInfoes { get; set; }
        public virtual DbSet<CompanyLocatorInfo> CompanyLocatorInfoes { get; set; }
        public virtual DbSet<CompanyProfile> CompanyProfiles { get; set; }
        public virtual DbSet<CompanyPublication> CompanyPublications { get; set; }
        public virtual DbSet<CompanyReview> CompanyReviews { get; set; }
        public virtual DbSet<CompanyReviewLog> CompanyReviewLogs { get; set; }
        public virtual DbSet<CompanyReviewResponse> CompanyReviewResponses { get; set; }
        public virtual DbSet<CompanyReviewResponseLog> CompanyReviewResponseLogs { get; set; }
        public virtual DbSet<CompanyReviewText> CompanyReviewTexts { get; set; }
        public virtual DbSet<CompanyScore> CompanyScores { get; set; }
        public virtual DbSet<CompanyScoreInfo> CompanyScoreInfoes { get; set; }
        public virtual DbSet<CompanySocialInfo> CompanySocialInfoes { get; set; }
        public virtual DbSet<CompanySocialProfile> CompanySocialProfiles { get; set; }
        public virtual DbSet<CompanyStatistic> CompanyStatistics { get; set; }
        public virtual DbSet<CompanyStat> CompanyStats { get; set; }
        public virtual DbSet<CountryState> CountryStates { get; set; }
        public virtual DbSet<EmailLog> EmailLogs { get; set; }
        public virtual DbSet<FlowResultHistoricalData> FlowResultHistoricalDatas { get; set; }
        public virtual DbSet<FootprintBuildsSet> FootprintBuildsSets { get; set; }
        public virtual DbSet<FootprintBuildState> FootprintBuildStates { get; set; }
        public virtual DbSet<FT_Accounts> FT_Accounts { get; set; }
        public virtual DbSet<GeneratedAdminReport> GeneratedAdminReports { get; set; }
        public virtual DbSet<GeneratedAgencyReport> GeneratedAgencyReports { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupPost> GroupPosts { get; set; }
        public virtual DbSet<GroupSubscriber> GroupSubscribers { get; set; }
        public virtual DbSet<HotelChain> HotelChains { get; set; }
        public virtual DbSet<Import> Imports { get; set; }
        public virtual DbSet<InfoProvider> InfoProviders { get; set; }
        public virtual DbSet<Invitation> Invitations { get; set; }
        public virtual DbSet<ListViewElement> ListViewElements { get; set; }
        public virtual DbSet<ListViewElementOrderGroup> ListViewElementOrderGroups { get; set; }
        public virtual DbSet<ListViewSet> ListViewSets { get; set; }
        public virtual DbSet<ListViewSetElement> ListViewSetElements { get; set; }
        public virtual DbSet<MessageTemplate> MessageTemplates { get; set; }
        public virtual DbSet<PartnerCommunity> PartnerCommunities { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<PointsPerPosition> PointsPerPositions { get; set; }
        public virtual DbSet<PricingFeature> PricingFeatures { get; set; }
        public virtual DbSet<PricingPlan> PricingPlans { get; set; }
        public virtual DbSet<PricingSubscription> PricingSubscriptions { get; set; }
        public virtual DbSet<PricingSubscriptionFeature> PricingSubscriptionFeatures { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<RequestSource> RequestSources { get; set; }
        public virtual DbSet<ScoreGroup> ScoreGroups { get; set; }
        public virtual DbSet<ScoreGroupValue> ScoreGroupValues { get; set; }
        public virtual DbSet<ScoreRule> ScoreRules { get; set; }
        public virtual DbSet<ScoreRuleDiff> ScoreRuleDiffs { get; set; }
        public virtual DbSet<ScoreRuleSet> ScoreRuleSets { get; set; }
        public virtual DbSet<ScoreRuleValue> ScoreRuleValues { get; set; }
        public virtual DbSet<ScoreStrength> ScoreStrengths { get; set; }
        public virtual DbSet<ScoreStrengthRange> ScoreStrengthRanges { get; set; }
        public virtual DbSet<SEOPosition> SEOPositions { get; set; }
        public virtual DbSet<Threshold> Thresholds { get; set; }
        public virtual DbSet<ThresholdReviewBound> ThresholdReviewBounds { get; set; }
        public virtual DbSet<UserFeedback> UserFeedbacks { get; set; }
        public virtual DbSet<UserFolder> UserFolders { get; set; }
        public virtual DbSet<UserFolderLocation> UserFolderLocations { get; set; }
        public virtual DbSet<UserFolderType> UserFolderTypes { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<W2GIChannelInfo> W2GIChannelInfo { get; set; }
        public virtual DbSet<ZipCode> ZipCodes { get; set; }
        public virtual DbSet<vwAgencyNextRefreshDate> vwAgencyNextRefreshDates { get; set; }
        public virtual DbSet<vwCompanyNextFullRefreshDate> vwCompanyNextFullRefreshDates { get; set; }
        public virtual DbSet<vwCompanyNextPartialRefreshDate> vwCompanyNextPartialRefreshDates { get; set; }
        public virtual DbSet<vwFootprintBuildState> vwFootprintBuildStates { get; set; }
        public virtual DbSet<vwfootPrintRefresh> vwfootPrintRefreshes { get; set; }
        public virtual DbSet<vwListView> vwListViews { get; set; }
        public virtual DbSet<vwListViewSetElement> vwListViewSetElements { get; set; }
        public virtual DbSet<vwScoreStrengthRange> vwScoreStrengthRanges { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgencyProfile>()
                .HasMany(e => e.AgencyCategories)
                .WithRequired(e => e.AgencyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgencyProfile>()
                .HasMany(e => e.AgencyRefreshes)
                .WithRequired(e => e.AgencyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgencyProfile>()
                .HasMany(e => e.AgencyReports)
                .WithRequired(e => e.AgencyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgencyProfile>()
                .HasOptional(e => e.AgencySetting)
                .WithRequired(e => e.AgencyProfile);

            modelBuilder.Entity<AgencyReport>()
                .HasMany(e => e.GeneratedAgencyReports)
                .WithRequired(e => e.AgencyReport)
                .HasForeignKey(e => new { e.AgencyId, e.ReportId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Community>()
                .HasMany(e => e.CommunitySubscribers)
                .WithRequired(e => e.Community)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Community>()
                .HasMany(e => e.CommunityTopics)
                .WithRequired(e => e.Community)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Community>()
                .HasMany(e => e.PartnerCommunities)
                .WithRequired(e => e.Community)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommunityTopic>()
                .HasMany(e => e.CommunityTopicFollowers)
                .WithRequired(e => e.CommunityTopic)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyBusinessType>()
                .HasMany(e => e.ScoreRules)
                .WithMany(e => e.CompanyBusinessTypes)
                .Map(m => m.ToTable("ScoreRuleBusinessType").MapLeftKey("BusinessTypeId").MapRightKey("RuleId"));

            modelBuilder.Entity<CompanyCategory>()
                .HasMany(e => e.CategorySEOPositions)
                .WithRequired(e => e.CompanyCategory)
                .HasForeignKey(e => e.CategoryId);

            modelBuilder.Entity<CompanyHistoricalInfo>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.LinkedInCompanyId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.FacebookPageId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.YelpBusinessId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.YahooId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.GoogleId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .Property(e => e.FoursquareCompanyId)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasOptional(e => e.CompanyAddress)
                .WithRequired(e => e.CompanyProfile);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyCategories)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyCompetitors)
                .WithRequired(e => e.CompanyProfile)
                .HasForeignKey(e => e.CompanyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyCompetitors1)
                .WithRequired(e => e.CompanyProfile1)
                .HasForeignKey(e => e.CompetitorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyDataQualityInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyDataQualityWarningCodesInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyHistoricalInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyLocatorInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyReviews)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanyScoreInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.CompanySocialInfoes)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasOptional(e => e.CompanyStatistic)
                .WithRequired(e => e.CompanyProfile);

            modelBuilder.Entity<CompanyProfile>()
                .HasOptional(e => e.CompanyStat)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.FlowResultHistoricalDatas)
                .WithRequired(e => e.CompanyProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.UserFolderLocations)
                .WithRequired(e => e.CompanyProfile)
                .HasForeignKey(e => e.LocationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyProfile>()
                .HasMany(e => e.UserProfiles)
                .WithOptional(e => e.CompanyProfile)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<CompanyReview>()
                .HasMany(e => e.CompanyReviewLogs)
                .WithRequired(e => e.CompanyReview)
                .HasForeignKey(e => e.ReviewId);

            modelBuilder.Entity<CompanyReview>()
                .HasMany(e => e.CompanyReviewResponses)
                .WithRequired(e => e.CompanyReview)
                .HasForeignKey(e => e.ReviewId);

            modelBuilder.Entity<CompanyReview>()
                .HasOptional(e => e.CompanyReviewText)
                .WithRequired(e => e.CompanyReview)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CompanyReviewResponse>()
                .HasMany(e => e.CompanyReviewResponseLogs)
                .WithRequired(e => e.CompanyReviewResponse)
                .HasForeignKey(e => e.ReviewResponseId);

            modelBuilder.Entity<FlowResultHistoricalData>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<FootprintBuildsSet>()
                .HasMany(e => e.FootprintBuildStates)
                .WithOptional(e => e.FootprintBuildsSet)
                .HasForeignKey(e => e.SetId);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CategorySEOPositions)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CompanyDataQualityWarningCodesInfoes)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CompanyHistoricalInfoes)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CompanyLocatorInfoes)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CompanyScoreInfoes)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.CompanySocialInfoes)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FootprintBuildState>()
                .HasMany(e => e.FlowResultHistoricalDatas)
                .WithRequired(e => e.FootprintBuildState)
                .HasForeignKey(e => e.FootprintBuildId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Group>()
                .HasMany(e => e.GroupPosts)
                .WithRequired(e => e.Group)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Group>()
                .HasMany(e => e.GroupSubscribers)
                .WithRequired(e => e.Group)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Import>()
                .HasMany(e => e.FootprintBuildStates)
                .WithOptional(e => e.Import)
                .HasForeignKey(e => e.ImportId);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.CategorySEOPositions)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.ProviderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.CompanyDataQualityInfoes)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.ProviderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.CompanyDataQualityWarningCodesInfoes)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.Provider)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.CompanyReviews)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.ProviderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.FlowResultHistoricalDatas)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.ProviderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InfoProvider>()
                .HasMany(e => e.ThresholdReviewBounds)
                .WithRequired(e => e.InfoProvider)
                .HasForeignKey(e => e.ProviderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invitation>()
                .Property(e => e.PartnerLogo)
                .IsUnicode(false);

            modelBuilder.Entity<ListViewElement>()
                .HasMany(e => e.ListViewSetElements)
                .WithRequired(e => e.ListViewElement)
                .HasForeignKey(e => e.ElementId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ListViewElementOrderGroup>()
                .HasMany(e => e.ListViewElements)
                .WithRequired(e => e.ListViewElementOrderGroup)
                .HasForeignKey(e => e.OrderGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ListViewSet>()
                .HasMany(e => e.ListViewSetElements)
                .WithRequired(e => e.ListViewSet)
                .HasForeignKey(e => e.SetId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Partner>()
                .Property(e => e.PartnerLogo)
                .IsUnicode(false);

            modelBuilder.Entity<Partner>()
                .HasMany(e => e.PartnerCommunities)
                .WithRequired(e => e.Partner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Partner>()
                .HasOptional(e => e.Partners1)
                .WithRequired(e => e.Partner1);

            modelBuilder.Entity<PricingFeature>()
                .HasMany(e => e.PricingSubscriptionFeatures)
                .WithRequired(e => e.PricingFeature)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PricingPlan>()
                .HasMany(e => e.PricingSubscriptions)
                .WithRequired(e => e.PricingPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PricingSubscription>()
                .HasMany(e => e.PricingSubscriptionFeatures)
                .WithRequired(e => e.PricingSubscription)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Report>()
                .HasMany(e => e.AgencyReports)
                .WithRequired(e => e.Report)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreGroup>()
                .HasMany(e => e.ScoreRules)
                .WithRequired(e => e.ScoreGroup)
                .HasForeignKey(e => e.GroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreGroup>()
                .HasMany(e => e.ScoreRuleValues)
                .WithRequired(e => e.ScoreGroup)
                .HasForeignKey(e => e.GroupId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreGroup>()
                .HasMany(e => e.ScoreStrengthRanges)
                .WithRequired(e => e.ScoreGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreRule>()
                .HasMany(e => e.ScoreRuleValues)
                .WithRequired(e => e.ScoreRule)
                .HasForeignKey(e => e.RuleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreRuleDiff>()
                .Property(e => e.DiffName)
                .IsUnicode(false);

            modelBuilder.Entity<ScoreRuleSet>()
                .HasMany(e => e.AgencyProfiles)
                .WithRequired(e => e.ScoreRuleSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreRuleSet>()
                .HasMany(e => e.CompanyScoreInfoes)
                .WithOptional(e => e.ScoreRuleSet)
                .HasForeignKey(e => e.RuleSetId);

            modelBuilder.Entity<ScoreRuleSet>()
                .HasMany(e => e.ScoreRuleValues)
                .WithRequired(e => e.ScoreRuleSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreRuleSet>()
                .HasMany(e => e.ScoreStrengthRanges)
                .WithRequired(e => e.ScoreRuleSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ScoreRuleValue>()
                .Property(e => e.ImageUrl)
                .IsUnicode(false);

            modelBuilder.Entity<ScoreStrength>()
                .HasMany(e => e.ScoreStrengthRanges)
                .WithRequired(e => e.ScoreStrength)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Threshold>()
                .HasMany(e => e.ThresholdReviewBounds)
                .WithRequired(e => e.Threshold)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserFolder>()
                .HasMany(e => e.UserFolderLocations)
                .WithRequired(e => e.UserFolder)
                .HasForeignKey(e => e.FolderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserFolderType>()
                .HasMany(e => e.UserFolders)
                .WithRequired(e => e.UserFolderType)
                .HasForeignKey(e => e.Type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.LinkedInProfileId)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.FacebookPageId)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.WindowsLiveId)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.RegistrationType)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.AgencyRefreshes)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.RequestorId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Communities)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CommunitySubscribers)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.SubscriberId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CommunityTopics)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CommunityTopicFollowers)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.FollowerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CompanyProfiles)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.CreatorId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CompanyPublications)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.CompanyReviewResponses)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Groups)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.Owner)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.GroupSubscribers)
                .WithRequired(e => e.UserProfile)
                .HasForeignKey(e => e.SubscriberId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Invitations)
                .WithOptional(e => e.UserProfile)
                .HasForeignKey(e => e.InviteeId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.Invitations1)
                .WithOptional(e => e.UserProfile1)
                .HasForeignKey(e => e.InviterId);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserFeedbacks)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .HasMany(e => e.UserFolders)
                .WithRequired(e => e.UserProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<W2GIChannelInfo>()
                .HasMany(e => e.CompanyLocatorInfoes)
                .WithRequired(e => e.W2GIChannelInfo)
                .HasForeignKey(e => e.ChannelId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<vwFootprintBuildState>()
                .Property(e => e.RequestSourceExt)
                .IsUnicode(false);

            modelBuilder.Entity<vwFootprintBuildState>()
                .Property(e => e.BuildTypeExt)
                .IsUnicode(false);

            modelBuilder.Entity<vwFootprintBuildState>()
                .Property(e => e.Duration)
                .HasPrecision(0);

            modelBuilder.Entity<vwfootPrintRefresh>()
                .Property(e => e.RequestSourceExt)
                .IsUnicode(false);

            modelBuilder.Entity<vwfootPrintRefresh>()
                .Property(e => e.BuildTypeExt)
                .IsUnicode(false);

            modelBuilder.Entity<vwfootPrintRefresh>()
                .Property(e => e.Duration)
                .HasPrecision(0);

            modelBuilder.Entity<vwListView>()
                .Property(e => e.CompleteAddress)
                .IsUnicode(false);
        }
    }
}
