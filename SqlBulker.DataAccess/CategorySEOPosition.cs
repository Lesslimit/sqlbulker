namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CategorySEOPosition")]
    public partial class CategorySEOPosition
    {
        public long Id { get; set; }

        public int CategoryId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public bool IsLast { get; set; }

        public int ProviderId { get; set; }

        public int Position { get; set; }

        public int? PositionDelta { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual CompanyCategory CompanyCategory { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }
    }
}
