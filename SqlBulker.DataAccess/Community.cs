namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Community")]
    public partial class Community
    {
        public Community()
        {
            CommunitySubscribers = new HashSet<CommunitySubscriber>();
            CommunityTopics = new HashSet<CommunityTopic>();
            PartnerCommunities = new HashSet<PartnerCommunity>();
        }

        public Guid CommunityId { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int Type { get; set; }

        public string Description { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid? AgencyId { get; set; }

        public Guid? DefaultForTeam { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<CommunitySubscriber> CommunitySubscribers { get; set; }

        public virtual ICollection<CommunityTopic> CommunityTopics { get; set; }

        public virtual ICollection<PartnerCommunity> PartnerCommunities { get; set; }
    }
}
