namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommunitySubscriber")]
    public partial class CommunitySubscriber
    {
        [Key]
        [Column(Order = 0)]
        public Guid CommunityId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid SubscriberId { get; set; }

        public DateTime DateSubscribed { get; set; }

        public int ActivityFeedPostFrequency { get; set; }

        public bool Subscribed { get; set; }

        public virtual Community Community { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
