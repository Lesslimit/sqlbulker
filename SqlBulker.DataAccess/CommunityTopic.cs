namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommunityTopic")]
    public partial class CommunityTopic
    {
        public CommunityTopic()
        {
            CommunityTopicFollowers = new HashSet<CommunityTopicFollower>();
        }

        [Key]
        public Guid TopicId { get; set; }

        public Guid CommunityId { get; set; }

        public Guid CreatorId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public int Type { get; set; }

        public DateTime DateCreated { get; set; }

        public int CommentsCount { get; set; }

        public bool Deleted { get; set; }

        public virtual Community Community { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<CommunityTopicFollower> CommunityTopicFollowers { get; set; }
    }
}
