namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommunityTopicFollower")]
    public partial class CommunityTopicFollower
    {
        [Key]
        [Column(Order = 0)]
        public Guid TopicId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid FollowerId { get; set; }

        public DateTime DateFollowed { get; set; }

        public virtual CommunityTopic CommunityTopic { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
