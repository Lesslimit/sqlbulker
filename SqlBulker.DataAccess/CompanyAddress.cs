namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyAddress")]
    public partial class CompanyAddress
    {
        [Key]
        public Guid CompanyId { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public double? Longitude { get; set; }

        public double? Latitude { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
