namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyBusinessType")]
    public partial class CompanyBusinessType
    {
        public CompanyBusinessType()
        {
            ScoreRules = new HashSet<ScoreRule>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<ScoreRule> ScoreRules { get; set; }
    }
}
