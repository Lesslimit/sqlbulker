namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyCategory")]
    public partial class CompanyCategory
    {
        public CompanyCategory()
        {
            CategorySEOPositions = new HashSet<CategorySEOPosition>();
        }

        public int Id { get; set; }

        public Guid CompanyId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public int? AgencyCategoryId { get; set; }

        public int DisplayOrder { get; set; }

        public int Status { get; set; }

        public virtual AgencyCategory AgencyCategory { get; set; }

        public virtual ICollection<CategorySEOPosition> CategorySEOPositions { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
