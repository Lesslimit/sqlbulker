namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyCompetitor")]
    public partial class CompanyCompetitor
    {
        [Key]
        [Column(Order = 0)]
        public Guid CompanyId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid CompetitorId { get; set; }

        public DateTime DateCreated { get; set; }

        public int CompetitorStatus { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual CompanyProfile CompanyProfile1 { get; set; }
    }
}
