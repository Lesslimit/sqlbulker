namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyDataQualityInfo")]
    public partial class CompanyDataQualityInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public int ProviderId { get; set; }

        public bool Connected { get; set; }

        public bool? Claimed { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string PhoneNumber { get; set; }

        [StringLength(500)]
        public string Website { get; set; }

        public string ProfileUrl { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }
    }
}
