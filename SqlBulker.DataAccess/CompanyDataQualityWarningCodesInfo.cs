namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyDataQualityWarningCodesInfo")]
    public partial class CompanyDataQualityWarningCodesInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public int Provider { get; set; }

        public bool IsLast { get; set; }

        public DateTime DateCreated { get; set; }

        public byte? WCNotConnected { get; set; }

        public byte? WCAddressDoesNotMatch { get; set; }

        public byte? WCAddress1DoesNotMatch { get; set; }

        public byte? WCAddress2DoesNotMatch { get; set; }

        public byte? WCCityDoesNotMatch { get; set; }

        public byte? WCStateDoesNotMatch { get; set; }

        public byte? WCZipCodeDoesNotMatch { get; set; }

        public byte? WCNoLogo { get; set; }

        public byte? WCNoReviews { get; set; }

        public byte? WCNotClaimed { get; set; }

        public byte? WCPhoneNumberDoesNotMatch { get; set; }

        public byte? WCWebsiteDoesNoMatch { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }
    }
}
