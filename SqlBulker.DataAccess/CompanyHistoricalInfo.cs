namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyHistoricalInfo")]
    public partial class CompanyHistoricalInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public bool IsLast { get; set; }

        public DateTime DateCreated { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(250)]
        public string LocationName { get; set; }

        [StringLength(250)]
        public string Website { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        public int CompanySize { get; set; }

        [StringLength(250)]
        public string Address1 { get; set; }

        [StringLength(250)]
        public string Address2 { get; set; }

        [StringLength(250)]
        public string City { get; set; }

        [StringLength(250)]
        public string State { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }
    }
}
