namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyLocatorInfo")]
    public partial class CompanyLocatorInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public int ChannelId { get; set; }

        public bool IsLast { get; set; }

        public DateTime? DateCreated { get; set; }

        public long DrivingDirections { get; set; }

        public long SendToEmail { get; set; }

        public long SendToPhone { get; set; }

        public long DrivingDirectionsChange { get; set; }

        public long SendToEmailChange { get; set; }

        public long SendToPhoneChange { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }

        public virtual W2GIChannelInfo W2GIChannelInfo { get; set; }
    }
}
