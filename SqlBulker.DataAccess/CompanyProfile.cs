namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyProfile")]
    public partial class CompanyProfile
    {
        public CompanyProfile()
        {
            CompanyCategories = new HashSet<CompanyCategory>();
            CompanyCompetitors = new HashSet<CompanyCompetitor>();
            CompanyCompetitors1 = new HashSet<CompanyCompetitor>();
            CompanyDataQualityInfoes = new HashSet<CompanyDataQualityInfo>();
            CompanyDataQualityWarningCodesInfoes = new HashSet<CompanyDataQualityWarningCodesInfo>();
            CompanyHistoricalInfoes = new HashSet<CompanyHistoricalInfo>();
            CompanyLocatorInfoes = new HashSet<CompanyLocatorInfo>();
            CompanyReviews = new HashSet<CompanyReview>();
            CompanyScoreInfoes = new HashSet<CompanyScoreInfo>();
            CompanySocialInfoes = new HashSet<CompanySocialInfo>();
            FlowResultHistoricalDatas = new HashSet<FlowResultHistoricalData>();
            UserFeedbacks = new HashSet<UserFeedback>();
            UserFolderLocations = new HashSet<UserFolderLocation>();
            UserProfiles = new HashSet<UserProfile>();
        }

        [Key]
        public Guid CompanyId { get; set; }

        [StringLength(50)]
        public string LinkedInCompanyId { get; set; }

        [StringLength(50)]
        public string FacebookPageId { get; set; }

        [StringLength(250)]
        public string YelpBusinessId { get; set; }

        [StringLength(50)]
        public string YahooId { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(250)]
        public string Website { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        public DateTime? DateCreated { get; set; }

        public int CompanySize { get; set; }

        public DateTime? LastPartialRefreshDate { get; set; }

        public DateTime? LastFullRefreshDate { get; set; }

        public int BusinessType { get; set; }

        public Guid? CreatorId { get; set; }

        [StringLength(50)]
        public string GoogleId { get; set; }

        public Guid? PartnerId { get; set; }

        public bool IsFacebookPagePrivate { get; set; }

        public Guid? AgencyId { get; set; }

        [StringLength(50)]
        public string FoursquareCompanyId { get; set; }

        public bool HasNewLocalSearchModel { get; set; }

        public bool HasNewSocialMediaModel { get; set; }

        public int SubscriberStatus { get; set; }

        [StringLength(50)]
        public string PayflowProfileId { get; set; }

        [StringLength(50)]
        public string TwitterId { get; set; }

        [StringLength(50)]
        public string GooglePlusId { get; set; }

        [StringLength(50)]
        public string YouTubeId { get; set; }

        [StringLength(50)]
        public string PinterestId { get; set; }

        public Guid? PricingPlanId { get; set; }

        public Guid? PricingSubscriptionId { get; set; }

        public DateTime? BillingNextPayment { get; set; }

        public bool? CompanyExpireMessageSent { get; set; }

        public bool? CompanyExpireWarnMessageSent { get; set; }

        [StringLength(250)]
        public string LocationName { get; set; }

        public int Status { get; set; }

        public bool? IsLocationRetrieved { get; set; }

        [StringLength(50)]
        public string W2CliendId { get; set; }

        [StringLength(50)]
        public string W2CompanyId { get; set; }

        public int? PartialRefreshFrequency { get; set; }

        public int? FullRefreshFrequency { get; set; }

        public int SearchMethod { get; set; }

        public Guid? ScoreRuleSetId { get; set; }

        public int? ReviewRefreshFrequency { get; set; }

        [StringLength(255)]
        public string LogoUrl { get; set; }

        public virtual CompanyAddress CompanyAddress { get; set; }

        public virtual ICollection<CompanyCategory> CompanyCategories { get; set; }

        public virtual ICollection<CompanyCompetitor> CompanyCompetitors { get; set; }

        public virtual ICollection<CompanyCompetitor> CompanyCompetitors1 { get; set; }

        public virtual ICollection<CompanyDataQualityInfo> CompanyDataQualityInfoes { get; set; }

        public virtual ICollection<CompanyDataQualityWarningCodesInfo> CompanyDataQualityWarningCodesInfoes { get; set; }

        public virtual ICollection<CompanyHistoricalInfo> CompanyHistoricalInfoes { get; set; }

        public virtual ICollection<CompanyLocatorInfo> CompanyLocatorInfoes { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<CompanyReview> CompanyReviews { get; set; }

        public virtual ICollection<CompanyScoreInfo> CompanyScoreInfoes { get; set; }

        public virtual ICollection<CompanySocialInfo> CompanySocialInfoes { get; set; }

        public virtual CompanyStatistic CompanyStatistic { get; set; }

        public virtual CompanyStat CompanyStat { get; set; }

        public virtual ICollection<FlowResultHistoricalData> FlowResultHistoricalDatas { get; set; }

        public virtual ICollection<UserFeedback> UserFeedbacks { get; set; }

        public virtual ICollection<UserFolderLocation> UserFolderLocations { get; set; }

        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
