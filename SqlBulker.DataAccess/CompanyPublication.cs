namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyPublication")]
    public partial class CompanyPublication
    {
        public Guid CompanyPublicationId { get; set; }

        public Guid CompanyId { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime PublishDate { get; set; }

        public DateTime? DatePublished { get; set; }

        public int Status { get; set; }

        public int PublishType { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
