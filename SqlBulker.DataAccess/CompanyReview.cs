namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyReview")]
    public partial class CompanyReview
    {
        public CompanyReview()
        {
            CompanyReviewLogs = new HashSet<CompanyReviewLog>();
            CompanyReviewResponses = new HashSet<CompanyReviewResponse>();
        }

        public long Id { get; set; }

        public Guid SolrId { get; set; }

        public Guid CompanyId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime ReviewDate { get; set; }

        public int ProviderId { get; set; }

        public double Rating { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        public string Reviewer { get; set; }

        public string URL { get; set; }

        [Required]
        [StringLength(200)]
        public string ProfileId { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }

        public virtual ICollection<CompanyReviewLog> CompanyReviewLogs { get; set; }

        public virtual ICollection<CompanyReviewResponse> CompanyReviewResponses { get; set; }

        public virtual CompanyReviewText CompanyReviewText { get; set; }
    }
}
