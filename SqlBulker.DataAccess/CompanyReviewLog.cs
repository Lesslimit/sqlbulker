namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyReviewLog")]
    public partial class CompanyReviewLog
    {
        public long Id { get; set; }

        public long ReviewId { get; set; }

        public DateTime DateCreated { get; set; }

        public ActionType Action { get; set; }

        public string Description { get; set; }

        public virtual CompanyReview CompanyReview { get; set; }
    }
}
