namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyReviewResponse")]
    public partial class CompanyReviewResponse
    {
        public CompanyReviewResponse()
        {
            CompanyReviewResponseLogs = new HashSet<CompanyReviewResponseLog>();
        }

        public long Id { get; set; }

        public Guid UserId { get; set; }

        public long ReviewId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? SubmittedDate { get; set; }

        [Required]
        public string Text { get; set; }

        public int Status { get; set; }

        public virtual CompanyReview CompanyReview { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<CompanyReviewResponseLog> CompanyReviewResponseLogs { get; set; }
    }
}
