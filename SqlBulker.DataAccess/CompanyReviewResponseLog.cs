namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyReviewResponseLog")]
    public partial class CompanyReviewResponseLog
    {
        public long Id { get; set; }

        public long ReviewResponseId { get; set; }

        public DateTime DateCreated { get; set; }

        public int Action { get; set; }

        public string Description { get; set; }

        public virtual CompanyReviewResponse CompanyReviewResponse { get; set; }
    }
}
