namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyScore")]
    public partial class CompanyScore
    {
        [Key]
        public Guid CompanyId { get; set; }

        public int? Score { get; set; }

        public int? BusinessScore { get; set; }

        public int? SocialScore { get; set; }

        public int? LocalSearchScore { get; set; }

        [StringLength(250)]
        public string Industry { get; set; }

        [StringLength(100)]
        public string BusinessType { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        public int CompanySize { get; set; }

        public int LocalRank { get; set; }

        public int NationalRank { get; set; }

        public int LocalPercentile { get; set; }

        public int NationalPercentile { get; set; }

        public int BusinessLocalRank { get; set; }

        public int BusinessNationalRank { get; set; }

        public int BusinessLocalPercentile { get; set; }

        public int BusinessNationalPercentile { get; set; }

        public int SocialLocalRank { get; set; }

        public int SocialNationalRank { get; set; }

        public int SocialLocalPercentile { get; set; }

        public int SocialNationalPercentile { get; set; }

        public int LocalSearchLocalRank { get; set; }

        public int LocalSearchNationalRank { get; set; }

        public int LocalSearchLocalPercentile { get; set; }

        public int LocalSearchNationalPercentile { get; set; }
    }
}
