namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyScoreInfo")]
    public partial class CompanyScoreInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public bool IsLast { get; set; }

        public DateTime DateCreated { get; set; }

        public int BrandScore { get; set; }

        public int BrandScoreDelta { get; set; }

        public int DataQualityScore { get; set; }

        public int DataQualityScoreDelta { get; set; }

        public int EngagementScore { get; set; }

        public int EngagementScoreDelta { get; set; }

        public int ReviewsScore { get; set; }

        public int ReviewsScoreDelta { get; set; }

        public int SearchScore { get; set; }

        public int SearchScoreDelta { get; set; }

        public Guid? RuleSetId { get; set; }

        public int? DataQualityMaxScore { get; set; }

        public int? EngagementMaxScore { get; set; }

        public int? ReviewsMaxScore { get; set; }

        public int? SearchMaxScore { get; set; }

        public int DataQualityMinScore { get; set; }

        public int EngagementMinScore { get; set; }

        public int ReviewsMinScore { get; set; }

        public int SearchMinScore { get; set; }

        public int ReviewsScoreStrength { get; set; }

        public int SearchScoreStrength { get; set; }

        public int DataQualityScoreStrength { get; set; }

        public int BrandScoreStrength { get; set; }

        public double ReviewsScorePercentage { get; set; }

        public double SearchScorePercentage { get; set; }

        public double DataQualityScorePercentage { get; set; }

        public double EngagementScorePercentage { get; set; }

        public int EngagementScoreStrength { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }

        public virtual ScoreRuleSet ScoreRuleSet { get; set; }
    }
}
