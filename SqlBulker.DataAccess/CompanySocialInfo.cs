namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanySocialInfo")]
    public partial class CompanySocialInfo
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public bool IsLast { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool? BingClaimed { get; set; }

        public long? BingReviews { get; set; }

        public long? BingReviewsDelta { get; set; }

        public double? BingUserRating { get; set; }

        public double? BingUserRatingDelta { get; set; }

        public long? FacebookCheckIns { get; set; }

        public long? FacebookCheckInsDelta { get; set; }

        public bool? FacebookClaimed { get; set; }

        public long? FacebookComments { get; set; }

        public long? FacebookCommentsDelta { get; set; }

        public long? FacebookLikes { get; set; }

        public long? FacebookLikesDelta { get; set; }

        public long? FacebookPosts { get; set; }

        public long? FacebookPostsDelta { get; set; }

        public long? FacebookTalkingAbout { get; set; }

        public long? FacebookTalkingAboutDelta { get; set; }

        public long? FoursquareCheckIns { get; set; }

        public long? FoursquareCheckInsDelta { get; set; }

        public bool? FoursquareClaimed { get; set; }

        public double? FoursquareRating { get; set; }

        public double? FoursquareRatingDelta { get; set; }

        public long? FoursquareTips { get; set; }

        public long? FoursquareTipsDelta { get; set; }

        public long? FoursquareUsers { get; set; }

        public long? FoursquareUsersDelta { get; set; }

        public long? GooglePlusActivities { get; set; }

        public long? GooglePlusActivitiesDelta { get; set; }

        public long? GooglePlusCircledBy { get; set; }

        public long? GooglePlusCircledByDelta { get; set; }

        public long? GooglePlusOne { get; set; }

        public long? GooglePlusOneDelta { get; set; }

        public double? GoogleRating { get; set; }

        public double? GoogleRatingDelta { get; set; }

        public long? LinkedInFollowers { get; set; }

        public long? LinkedInFollowersDelta { get; set; }

        public long? PinterestFollowers { get; set; }

        public long? PinterestFollowersDelta { get; set; }

        public long? PinterestPins { get; set; }

        public long? PinterestPinsDelta { get; set; }

        public long? SEMRushAdsTraffic { get; set; }

        public long? SEMRushAdsTrafficDelta { get; set; }

        public long? SEMRushSearchTraffic { get; set; }

        public long? SEMRushSearchTrafficDelta { get; set; }

        public long? TripAdvisorAreaRank { get; set; }

        public long? TripAdvisorAreaRankDelta { get; set; }

        public double? TripAdvisorRating { get; set; }

        public double? TripAdvisorRatingDelta { get; set; }

        public long? TripAdvisorReviews { get; set; }

        public long? TripAdvisorReviewsDelta { get; set; }

        public long? TwitterFollowers { get; set; }

        public long? TwitterFollowersDelta { get; set; }

        public long? TwitterFriends { get; set; }

        public long? TwitterFriendsDelta { get; set; }

        public long? TwitterListed { get; set; }

        public long? TwitterListedDelta { get; set; }

        public long? TwitterMentions { get; set; }

        public long? TwitterMentionsDelta { get; set; }

        public long? TwitterStatus { get; set; }

        public long? TwitterStatusDelta { get; set; }

        public double? YahooAverageRating { get; set; }

        public double? YahooAverageRatingDelta { get; set; }

        public long? YahooTotalRatings { get; set; }

        public long? YahooTotalRatingsDelta { get; set; }

        public long? YahooTotalReviews { get; set; }

        public long? YahooTotalReviewsDelta { get; set; }

        public bool? YelpClaimed { get; set; }

        public double? YelpRating { get; set; }

        public double? YelpRatingDelta { get; set; }

        public long? YelpReviews { get; set; }

        public long? YelpReviewsDelta { get; set; }

        public long? YouTubeFavorites { get; set; }

        public long? YouTubeFavoritesDelta { get; set; }

        public long? YouTubeSubscribers { get; set; }

        public long? YouTubeSubscribersDelta { get; set; }

        public long? YouTubeViews { get; set; }

        public long? YouTubeViewsDelta { get; set; }

        public double? YellowPagesRating { get; set; }

        public double? MerchantCircleRating { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }
    }
}
