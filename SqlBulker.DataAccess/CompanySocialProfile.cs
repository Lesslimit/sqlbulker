namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanySocialProfile")]
    public partial class CompanySocialProfile
    {
        [Key]
        public Guid CompanyId { get; set; }

        [StringLength(500)]
        public string Facebook { get; set; }

        [StringLength(500)]
        public string Facebook_Homepage { get; set; }

        [StringLength(500)]
        public string Twitter { get; set; }

        [StringLength(500)]
        public string Twitter_Homepage { get; set; }

        [StringLength(500)]
        public string LinkedIn { get; set; }

        [StringLength(500)]
        public string LinkedIn_Homepage { get; set; }

        [StringLength(500)]
        public string GooglePlus { get; set; }

        [StringLength(500)]
        public string GooglePlus_Homepage { get; set; }

        [StringLength(500)]
        public string Pinterest { get; set; }

        [StringLength(500)]
        public string Pinterest_Homepage { get; set; }

        [StringLength(500)]
        public string YouTube { get; set; }

        [StringLength(500)]
        public string YouTube_Homepage { get; set; }

        [StringLength(250)]
        public string Website { get; set; }

        public bool AddThisFound { get; set; }

        [StringLength(1000)]
        public string AddThisPubID { get; set; }

        public bool GoogleAnalyticsFound { get; set; }

        public int BusinessType { get; set; }

        public DateTime? DateUpdated { get; set; }
    }
}
