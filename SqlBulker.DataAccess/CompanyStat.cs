namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompanyStat
    {
        [Key]
        public Guid CompanyId { get; set; }

        public int BusinessType { get; set; }

        public int CompanySize { get; set; }

        public long? FacebookLikes { get; set; }

        public long FoursquareCheckins { get; set; }

        public long LinkedInFollowers { get; set; }

        public long TwitterFollowers { get; set; }

        public long YouTubeSubscribers { get; set; }

        public long FacebookCheckins { get; set; }

        public long FourSquareUserCount { get; set; }

        public long GooglePlusOneCount { get; set; }

        public long GoogleCircledByCount { get; set; }

        public long PinterestFollowers { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
