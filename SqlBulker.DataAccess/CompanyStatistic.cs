namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyStatistic")]
    public partial class CompanyStatistic
    {
        [Key]
        public Guid CompanyId { get; set; }

        public DateTime? FacebookDateAttached { get; set; }

        public long? FacebookLikesCount { get; set; }

        public long? FacebookCheckinsCount { get; set; }

        public DateTime? TwitterDateAttached { get; set; }

        public long? TwitterFollowersCount { get; set; }

        public DateTime? FoursquareDateAttached { get; set; }

        public long? FoursquareCheckinsCount { get; set; }

        public long? FoursquareUsersCount { get; set; }

        public DateTime? LinkedInDateAttached { get; set; }

        public long? LinkedInFollowersCount { get; set; }

        [StringLength(5)]
        public string CompanyState { get; set; }

        public bool? DescriptionTag { get; set; }

        public bool? TitleTag { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }
    }
}
