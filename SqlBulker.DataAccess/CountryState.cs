namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CountryState")]
    public partial class CountryState
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string CountryCode { get; set; }

        [StringLength(50)]
        public string CountryName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string StateCode { get; set; }

        [StringLength(50)]
        public string StateName { get; set; }
    }
}
