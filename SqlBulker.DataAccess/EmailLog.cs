namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmailLog")]
    public partial class EmailLog
    {
        public int Id { get; set; }

        public Guid? UserId { get; set; }

        [Required]
        [StringLength(200)]
        public string TemplateName { get; set; }

        [Required]
        [StringLength(200)]
        public string EmailAddress { get; set; }

        public DateTime DateSent { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
