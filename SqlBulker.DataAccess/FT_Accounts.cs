namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FT_Accounts
    {
        [Key]
        public Guid FT_Id { get; set; }

        public Guid? FT_UserGUID { get; set; }

        public DateTime FT_ContactMeDate { get; set; }

        [Required]
        public string FT_ContactMeTel { get; set; }

        [Required]
        public string FT_CustomerStatus { get; set; }

        public string FT_AcctManager { get; set; }

        public DateTime? FT_ClosedDate { get; set; }

        public string FT_BidURL { get; set; }
    }
}
