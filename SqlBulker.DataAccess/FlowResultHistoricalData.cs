namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FlowResultHistoricalData")]
    public partial class FlowResultHistoricalData
    {
        public long Id { get; set; }

        public Guid CompanyId { get; set; }

        public Guid FootprintBuildId { get; set; }

        public int ProviderId { get; set; }

        public bool IsLast { get; set; }

        public DateTime DateCreated { get; set; }

        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(250)]
        public string WebSite { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public bool? IsClaimed { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual FootprintBuildState FootprintBuildState { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }
    }
}
