namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FootprintBuildState")]
    public partial class FootprintBuildState
    {
        public FootprintBuildState()
        {
            CategorySEOPositions = new HashSet<CategorySEOPosition>();
            CompanyDataQualityWarningCodesInfoes = new HashSet<CompanyDataQualityWarningCodesInfo>();
            CompanyHistoricalInfoes = new HashSet<CompanyHistoricalInfo>();
            CompanyLocatorInfoes = new HashSet<CompanyLocatorInfo>();
            CompanyScoreInfoes = new HashSet<CompanyScoreInfo>();
            CompanySocialInfoes = new HashSet<CompanySocialInfo>();
            FlowResultHistoricalDatas = new HashSet<FlowResultHistoricalData>();
        }

        public Guid Id { get; set; }

        public Guid CompanyProfileId { get; set; }

        public int DataDiscoveryState { get; set; }

        public DateTime? DataDiscoveryTimeStamp { get; set; }

        public int ScoringState { get; set; }

        public DateTime? ScoringTimeStamp { get; set; }

        public int MetricsState { get; set; }

        public DateTime? MetricsTimeStamp { get; set; }

        public int RequestSource { get; set; }

        public int BuildType { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid? ImportId { get; set; }

        public bool IsCleaned { get; set; }

        public Guid? SetId { get; set; }

        public bool isLast { get; set; }

        public virtual ICollection<CategorySEOPosition> CategorySEOPositions { get; set; }

        public virtual ICollection<CompanyDataQualityWarningCodesInfo> CompanyDataQualityWarningCodesInfoes { get; set; }

        public virtual ICollection<CompanyHistoricalInfo> CompanyHistoricalInfoes { get; set; }

        public virtual ICollection<CompanyLocatorInfo> CompanyLocatorInfoes { get; set; }

        public virtual ICollection<CompanyScoreInfo> CompanyScoreInfoes { get; set; }

        public virtual ICollection<CompanySocialInfo> CompanySocialInfoes { get; set; }

        public virtual ICollection<FlowResultHistoricalData> FlowResultHistoricalDatas { get; set; }

        public virtual FootprintBuildsSet FootprintBuildsSet { get; set; }

        public virtual Import Import { get; set; }
    }
}
