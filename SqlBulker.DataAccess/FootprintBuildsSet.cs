namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FootprintBuildsSet")]
    public partial class FootprintBuildsSet
    {
        public FootprintBuildsSet()
        {
            AgencyRefreshes = new HashSet<AgencyRefresh>();
            FootprintBuildStates = new HashSet<FootprintBuildState>();
        }

        public Guid Id { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<AgencyRefresh> AgencyRefreshes { get; set; }

        public virtual ICollection<FootprintBuildState> FootprintBuildStates { get; set; }
    }
}
