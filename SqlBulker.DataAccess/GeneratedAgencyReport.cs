namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneratedAgencyReport")]
    public partial class GeneratedAgencyReport
    {
        public Guid Id { get; set; }

        public Guid AgencyId { get; set; }

        public Guid ReportId { get; set; }

        [Required]
        [StringLength(100)]
        public string FileName { get; set; }

        public DateTime DateCreated { get; set; }

        public int Status { get; set; }

        public int ReportFormat { get; set; }

        public DateTime? DateFinished { get; set; }

        public virtual AgencyReport AgencyReport { get; set; }
    }
}
