namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Group")]
    public partial class Group
    {
        public Group()
        {
            GroupPosts = new HashSet<GroupPost>();
            GroupSubscribers = new HashSet<GroupSubscriber>();
        }

        public Guid GroupId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [StringLength(200)]
        public string IconUrl { get; set; }

        public Guid Owner { get; set; }

        public int PostFrequency { get; set; }

        public int TotalSubscribers { get; set; }

        public DateTime? LastPostDate { get; set; }

        public int Type { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<GroupPost> GroupPosts { get; set; }

        public virtual ICollection<GroupSubscriber> GroupSubscribers { get; set; }
    }
}
