namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GroupPost")]
    public partial class GroupPost
    {
        [Key]
        public Guid PostId { get; set; }

        public Guid GroupId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public string FullText { get; set; }

        [StringLength(200)]
        public string Url { get; set; }

        [StringLength(200)]
        public string IconUrl { get; set; }

        public DateTime Date { get; set; }

        public bool CommentsAllowed { get; set; }

        public virtual Group Group { get; set; }
    }
}
