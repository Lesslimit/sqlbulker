namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GroupSubscriber")]
    public partial class GroupSubscriber
    {
        [Key]
        [Column(Order = 0)]
        public Guid GroupId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid SubscriberId { get; set; }

        public DateTime DateSubscribed { get; set; }

        public bool ShowInFeed { get; set; }

        public virtual Group Group { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
