namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HotelChain
    {
        [Key]
        public int ChainId { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }
    }
}
