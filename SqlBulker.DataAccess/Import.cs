namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Import")]
    public partial class Import
    {
        public Import()
        {
            FootprintBuildStates = new HashSet<FootprintBuildState>();
        }

        [Key]
        public Guid ReportId { get; set; }

        public Guid? AgencyId { get; set; }

        [StringLength(255)]
        public string AgencyName { get; set; }

        [StringLength(255)]
        public string UserEmail { get; set; }

        [StringLength(255)]
        public string Password { get; set; }

        public int TotalRecords { get; set; }

        public int SkippedRecords { get; set; }

        public int CompaniesCreated { get; set; }

        public int CompaniesUpdated { get; set; }

        public int CompaniesDeleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateFinished { get; set; }

        public int FootprintBuildsSucceeded { get; set; }

        public int FootprintBuildsFailed { get; set; }

        public int ErroredRecords { get; set; }

        [Required]
        [StringLength(250)]
        public string ReportName { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        public bool isTestAgency { get; set; }

        public int Status { get; set; }

        public int FootprintBuildsCanceled { get; set; }

        public virtual ICollection<FootprintBuildState> FootprintBuildStates { get; set; }
    }
}
