namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InfoProvider")]
    public partial class InfoProvider
    {
        public InfoProvider()
        {
            CategorySEOPositions = new HashSet<CategorySEOPosition>();
            CompanyDataQualityInfoes = new HashSet<CompanyDataQualityInfo>();
            CompanyDataQualityWarningCodesInfoes = new HashSet<CompanyDataQualityWarningCodesInfo>();
            CompanyReviews = new HashSet<CompanyReview>();
            FlowResultHistoricalDatas = new HashSet<FlowResultHistoricalData>();
            ThresholdReviewBounds = new HashSet<ThresholdReviewBound>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<CategorySEOPosition> CategorySEOPositions { get; set; }

        public virtual ICollection<CompanyDataQualityInfo> CompanyDataQualityInfoes { get; set; }

        public virtual ICollection<CompanyDataQualityWarningCodesInfo> CompanyDataQualityWarningCodesInfoes { get; set; }

        public virtual ICollection<CompanyReview> CompanyReviews { get; set; }

        public virtual ICollection<FlowResultHistoricalData> FlowResultHistoricalDatas { get; set; }

        public virtual ICollection<ThresholdReviewBound> ThresholdReviewBounds { get; set; }
    }
}
