namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invitation")]
    public partial class Invitation
    {
        [Key]
        public int Invitation_Id { get; set; }

        public Guid InvitationCode { get; set; }

        public DateTime InvitationDate { get; set; }

        public Guid? InviterId { get; set; }

        [Required]
        public string InviteeEmail { get; set; }

        public Guid? InviteeId { get; set; }

        public DateTime? ResponseDate { get; set; }

        public int Status { get; set; }

        public int Type { get; set; }

        public Guid? ExternalId { get; set; }

        public Guid? ConfirmationCode { get; set; }

        public Guid? PartnerId { get; set; }

        [StringLength(100)]
        public string PartnerLogo { get; set; }

        public virtual Partner Partner { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual UserProfile UserProfile1 { get; set; }
    }
}
