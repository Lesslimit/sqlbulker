namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ListViewElement")]
    public partial class ListViewElement
    {
        public ListViewElement()
        {
            ListViewSetElements = new HashSet<ListViewSetElement>();
        }

        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FieldName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public string FullDescription { get; set; }

        [StringLength(50)]
        public string CssClass { get; set; }

        public int OrderGroup { get; set; }

        public DateTime DateCreated { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsEditable { get; set; }

        public int ComparerType { get; set; }

        [StringLength(5)]
        public string ValueSuffix { get; set; }

        public virtual ListViewElementOrderGroup ListViewElementOrderGroup { get; set; }

        public virtual ICollection<ListViewSetElement> ListViewSetElements { get; set; }
    }
}
