namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ListViewElementOrderGroup")]
    public partial class ListViewElementOrderGroup
    {
        public ListViewElementOrderGroup()
        {
            ListViewElements = new HashSet<ListViewElement>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<ListViewElement> ListViewElements { get; set; }
    }
}
