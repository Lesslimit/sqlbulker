namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ListViewSet")]
    public partial class ListViewSet
    {
        public ListViewSet()
        {
            ListViewSetElements = new HashSet<ListViewSetElement>();
        }

        [StringLength(20)]
        public string Id { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<ListViewSetElement> ListViewSetElements { get; set; }
    }
}
