namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ListViewSetElement")]
    public partial class ListViewSetElement
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string SetId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ElementId { get; set; }

        [StringLength(50)]
        public string FieldName { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsDefault { get; set; }

        public int DisplayOrder { get; set; }

        public virtual ListViewElement ListViewElement { get; set; }

        public virtual ListViewSet ListViewSet { get; set; }
    }
}
