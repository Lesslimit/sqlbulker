namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageTemplate")]
    public partial class MessageTemplate
    {
        [Key]
        public int MessageTemplate_Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Subject { get; set; }

        [Required]
        public string HtmlBody { get; set; }

        [Required]
        public string TextBody { get; set; }

        public Guid? PartnerId { get; set; }

        public virtual Partner Partner { get; set; }
    }
}
