namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Partner
    {
        public Partner()
        {
            Invitations = new HashSet<Invitation>();
            MessageTemplates = new HashSet<MessageTemplate>();
            PartnerCommunities = new HashSet<PartnerCommunity>();
        }

        public Guid PartnerId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string HomePage { get; set; }

        [StringLength(100)]
        public string PartnerLogo { get; set; }

        public bool DashboardOnly { get; set; }

        public bool ConfirmationRequired { get; set; }

        public Guid? PlanId { get; set; }

        public bool IsObsolete { get; set; }

        public Guid? ScoreRuleSetId { get; set; }

        public virtual ICollection<Invitation> Invitations { get; set; }

        public virtual ICollection<MessageTemplate> MessageTemplates { get; set; }

        public virtual ICollection<PartnerCommunity> PartnerCommunities { get; set; }

        public virtual Partner Partners1 { get; set; }

        public virtual Partner Partner1 { get; set; }
    }
}
