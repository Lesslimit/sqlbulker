namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PartnerCommunity")]
    public partial class PartnerCommunity
    {
        [Key]
        [Column(Order = 0)]
        public Guid PartnerId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid CommunityId { get; set; }

        public DateTime DateAssigned { get; set; }

        public virtual Community Community { get; set; }

        public virtual Partner Partner { get; set; }
    }
}
