namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PointsPerPosition")]
    public partial class PointsPerPosition
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Position { get; set; }

        public int Points { get; set; }
    }
}
