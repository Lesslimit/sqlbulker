namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PricingFeature")]
    public partial class PricingFeature
    {
        public PricingFeature()
        {
            PricingSubscriptionFeatures = new HashSet<PricingSubscriptionFeature>();
        }

        [Key]
        public Guid FeatureId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<PricingSubscriptionFeature> PricingSubscriptionFeatures { get; set; }
    }
}
