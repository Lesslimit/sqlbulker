namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PricingPlan")]
    public partial class PricingPlan
    {
        public PricingPlan()
        {
            PricingSubscriptions = new HashSet<PricingSubscription>();
        }

        [Key]
        public Guid PlanId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public int TrialPeriod { get; set; }

        public bool IsDefault { get; set; }

        public virtual ICollection<PricingSubscription> PricingSubscriptions { get; set; }
    }
}
