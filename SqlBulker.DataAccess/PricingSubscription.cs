namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PricingSubscription")]
    public partial class PricingSubscription
    {
        public PricingSubscription()
        {
            PricingSubscriptionFeatures = new HashSet<PricingSubscriptionFeature>();
        }

        [Key]
        public Guid SubscriptionId { get; set; }

        public Guid PlanId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public double Price { get; set; }

        public DateTime DateCreated { get; set; }

        public bool Selectable { get; set; }

        public bool Deleted { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsDefault { get; set; }

        public virtual PricingPlan PricingPlan { get; set; }

        public virtual ICollection<PricingSubscriptionFeature> PricingSubscriptionFeatures { get; set; }
    }
}
