namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PricingSubscriptionFeature")]
    public partial class PricingSubscriptionFeature
    {
        [Key]
        [Column(Order = 0)]
        public Guid SubscriptionId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid FeatureId { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual PricingFeature PricingFeature { get; set; }

        public virtual PricingSubscription PricingSubscription { get; set; }
    }
}
