namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Report")]
    public partial class Report
    {
        public Report()
        {
            AgencyReports = new HashSet<AgencyReport>();
        }

        public Guid ReportId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        [StringLength(50)]
        public string MethodName { get; set; }

        public int Type { get; set; }

        public bool IsDefault { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<AgencyReport> AgencyReports { get; set; }
    }
}
