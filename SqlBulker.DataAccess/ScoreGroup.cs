namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreGroup")]
    public partial class ScoreGroup
    {
        public ScoreGroup()
        {
            ScoreRules = new HashSet<ScoreRule>();
            ScoreRuleValues = new HashSet<ScoreRuleValue>();
            ScoreStrengthRanges = new HashSet<ScoreStrengthRange>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<ScoreRule> ScoreRules { get; set; }

        public virtual ICollection<ScoreRuleValue> ScoreRuleValues { get; set; }

        public virtual ICollection<ScoreStrengthRange> ScoreStrengthRanges { get; set; }
    }
}
