namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreGroupValue")]
    public partial class ScoreGroupValue
    {
        [Key]
        public int ScoreGroupValue_Id { get; set; }

        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        [StringLength(100)]
        public string LowerBound { get; set; }

        [StringLength(100)]
        public string UpperBound { get; set; }

        public int Score { get; set; }

        public string Comment { get; set; }

        public string Recommendation { get; set; }

        public int ScoreTypeInt { get; set; }
    }
}
