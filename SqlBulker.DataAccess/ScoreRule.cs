namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreRule")]
    public partial class ScoreRule
    {
        public ScoreRule()
        {
            ScoreRuleValues = new HashSet<ScoreRuleValue>();
            CompanyBusinessTypes = new HashSet<CompanyBusinessType>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int GroupId { get; set; }

        public DateTime DateCreated { get; set; }

        public int Status { get; set; }

        public virtual ScoreGroup ScoreGroup { get; set; }

        public virtual ICollection<ScoreRuleValue> ScoreRuleValues { get; set; }

        public virtual ICollection<CompanyBusinessType> CompanyBusinessTypes { get; set; }
    }
}
