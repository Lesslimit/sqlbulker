namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreRuleDiff")]
    public partial class ScoreRuleDiff
    {
        [Key]
        [StringLength(50)]
        public string DiffName { get; set; }

        [StringLength(255)]
        public string PositiveValueChange { get; set; }

        [StringLength(255)]
        public string NegativeValueChange { get; set; }

        [StringLength(255)]
        public string PositiveScoreChange { get; set; }

        [StringLength(255)]
        public string NegativeScoreChange { get; set; }

        [StringLength(255)]
        public string NewPropertyText { get; set; }

        [Required]
        [StringLength(10)]
        public string Type { get; set; }
    }
}
