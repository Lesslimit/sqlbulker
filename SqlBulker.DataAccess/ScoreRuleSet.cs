namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreRuleSet")]
    public partial class ScoreRuleSet
    {
        public ScoreRuleSet()
        {
            AgencyProfiles = new HashSet<AgencyProfile>();
            CompanyScoreInfoes = new HashSet<CompanyScoreInfo>();
            ScoreRuleValues = new HashSet<ScoreRuleValue>();
            ScoreStrengthRanges = new HashSet<ScoreStrengthRange>();
        }

        public Guid Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public bool IsDefault { get; set; }

        public virtual ICollection<AgencyProfile> AgencyProfiles { get; set; }

        public virtual ICollection<CompanyScoreInfo> CompanyScoreInfoes { get; set; }

        public virtual ICollection<ScoreRuleValue> ScoreRuleValues { get; set; }

        public virtual ICollection<ScoreStrengthRange> ScoreStrengthRanges { get; set; }
    }
}
