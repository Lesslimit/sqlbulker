namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreRuleValue")]
    public partial class ScoreRuleValue
    {
        [Key]
        public int ScoreRuleValue_Id { get; set; }

        [Required]
        [StringLength(100)]
        public string RuleName { get; set; }

        [StringLength(100)]
        public string LowerBound { get; set; }

        [StringLength(100)]
        public string UpperBound { get; set; }

        public int Score { get; set; }

        public string Comment { get; set; }

        public string Recommendation { get; set; }

        [StringLength(200)]
        public string ImageUrl { get; set; }

        public int ScoreTypeInt { get; set; }

        [StringLength(100)]
        public string LinkUrl { get; set; }

        public Guid RuleUniqueId { get; set; }

        [StringLength(200)]
        public string W8ImageUrl { get; set; }

        public string W8RecommendationText { get; set; }

        [StringLength(200)]
        public string W8RecommendationDetailUrl { get; set; }

        public string KnowledgeUrl { get; set; }

        public string FixThisUrl { get; set; }

        public bool ShowRecommendation { get; set; }

        public Guid ScoreRuleSetId { get; set; }

        public int GroupId { get; set; }

        public int RuleId { get; set; }

        public virtual ScoreGroup ScoreGroup { get; set; }

        public virtual ScoreRule ScoreRule { get; set; }

        public virtual ScoreRuleSet ScoreRuleSet { get; set; }
    }
}
