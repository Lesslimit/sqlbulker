namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ScoreStrengthRange")]
    public partial class ScoreStrengthRange
    {
        public int Id { get; set; }

        public Guid ScoreRuleSetId { get; set; }

        public int ScoreGroupId { get; set; }

        public int ScoreStrengthId { get; set; }

        public double LowerBound { get; set; }

        public double UpperBound { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ScoreGroup ScoreGroup { get; set; }

        public virtual ScoreRuleSet ScoreRuleSet { get; set; }

        public virtual ScoreStrength ScoreStrength { get; set; }
    }
}
