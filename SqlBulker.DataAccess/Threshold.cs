namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Threshold")]
    public partial class Threshold
    {
        public Threshold()
        {
            AgencySettings = new HashSet<AgencySetting>();
            ThresholdReviewBounds = new HashSet<ThresholdReviewBound>();
        }

        public int Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<AgencySetting> AgencySettings { get; set; }

        public virtual ICollection<ThresholdReviewBound> ThresholdReviewBounds { get; set; }
    }
}
