namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThresholdReviewBound")]
    public partial class ThresholdReviewBound
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ThresholdId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProviderId { get; set; }

        public double Value { get; set; }

        public virtual InfoProvider InfoProvider { get; set; }

        public virtual Threshold Threshold { get; set; }
    }
}
