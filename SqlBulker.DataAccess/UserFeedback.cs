namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserFeedback")]
    public partial class UserFeedback
    {
        [Key]
        public int FeedbackId { get; set; }

        public Guid UserId { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        public string Text { get; set; }

        public Guid? CompanyId { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
