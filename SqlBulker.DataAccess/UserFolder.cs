namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserFolder")]
    public partial class UserFolder
    {
        public UserFolder()
        {
            UserFolderLocations = new HashSet<UserFolderLocation>();
        }

        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Type { get; set; }

        public string SQL { get; set; }

        public DateTime DateCreated { get; set; }

        public Guid? AgencyId { get; set; }

        public virtual AgencyProfile AgencyProfile { get; set; }

        public virtual UserFolderType UserFolderType { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<UserFolderLocation> UserFolderLocations { get; set; }
    }
}
