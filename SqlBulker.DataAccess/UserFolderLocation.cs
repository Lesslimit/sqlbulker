namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserFolderLocation")]
    public partial class UserFolderLocation
    {
        [Key]
        [Column(Order = 0)]
        public Guid FolderId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid LocationId { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual UserFolder UserFolder { get; set; }
    }
}
