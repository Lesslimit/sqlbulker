namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserProfile")]
    public partial class UserProfile
    {
        public UserProfile()
        {
            AgencyRefreshes = new HashSet<AgencyRefresh>();
            Communities = new HashSet<Community>();
            CommunitySubscribers = new HashSet<CommunitySubscriber>();
            CommunityTopics = new HashSet<CommunityTopic>();
            CommunityTopicFollowers = new HashSet<CommunityTopicFollower>();
            CompanyProfiles = new HashSet<CompanyProfile>();
            CompanyPublications = new HashSet<CompanyPublication>();
            CompanyReviewResponses = new HashSet<CompanyReviewResponse>();
            EmailLogs = new HashSet<EmailLog>();
            Groups = new HashSet<Group>();
            GroupSubscribers = new HashSet<GroupSubscriber>();
            Invitations = new HashSet<Invitation>();
            Invitations1 = new HashSet<Invitation>();
            UserFeedbacks = new HashSet<UserFeedback>();
            UserFolders = new HashSet<UserFolder>();
        }

        [Key]
        public Guid UserId { get; set; }

        public Guid? CompanyId { get; set; }

        [StringLength(50)]
        public string LinkedInProfileId { get; set; }

        [StringLength(50)]
        public string FacebookPageId { get; set; }

        [StringLength(50)]
        public string WindowsLiveId { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [StringLength(20)]
        public string MyWebCareerId { get; set; }

        public int Status { get; set; }

        [StringLength(50)]
        public string CampaignCode { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public int TotalLogins { get; set; }

        [StringLength(255)]
        public string PasswordHash { get; set; }

        public Guid? AgencyId { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(12)]
        public string RegistrationType { get; set; }

        [StringLength(50)]
        public string UserKey { get; set; }

        public virtual ICollection<AgencyRefresh> AgencyRefreshes { get; set; }

        public virtual ICollection<Community> Communities { get; set; }

        public virtual ICollection<CommunitySubscriber> CommunitySubscribers { get; set; }

        public virtual ICollection<CommunityTopic> CommunityTopics { get; set; }

        public virtual ICollection<CommunityTopicFollower> CommunityTopicFollowers { get; set; }

        public virtual ICollection<CompanyProfile> CompanyProfiles { get; set; }

        public virtual CompanyProfile CompanyProfile { get; set; }

        public virtual ICollection<CompanyPublication> CompanyPublications { get; set; }

        public virtual ICollection<CompanyReviewResponse> CompanyReviewResponses { get; set; }

        public virtual ICollection<EmailLog> EmailLogs { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        public virtual ICollection<GroupSubscriber> GroupSubscribers { get; set; }

        public virtual ICollection<Invitation> Invitations { get; set; }

        public virtual ICollection<Invitation> Invitations1 { get; set; }

        public virtual ICollection<UserFeedback> UserFeedbacks { get; set; }

        public virtual ICollection<UserFolder> UserFolders { get; set; }
    }
}
