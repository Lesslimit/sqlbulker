namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class W2GIChannelInfo
    {
        public W2GIChannelInfo()
        {
            CompanyLocatorInfoes = new HashSet<CompanyLocatorInfo>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual ICollection<CompanyLocatorInfo> CompanyLocatorInfoes { get; set; }
    }
}
