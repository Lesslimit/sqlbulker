namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ZipCode")]
    public partial class ZipCode
    {
        public int Id { get; set; }

        [Column("ZipCode")]
        [StringLength(5)]
        public string ZipCode1 { get; set; }

        [StringLength(35)]
        public string City { get; set; }

        [StringLength(2)]
        public string State { get; set; }

        [StringLength(45)]
        public string County { get; set; }

        [StringLength(45)]
        public string AreaCode { get; set; }

        [StringLength(2)]
        public string TimeZone { get; set; }

        [StringLength(1)]
        public string DayLightSaving { get; set; }

        [StringLength(20)]
        public string Latitude { get; set; }

        [StringLength(20)]
        public string Longitude { get; set; }
    }
}
