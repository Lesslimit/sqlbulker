namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwAgencyNextRefreshDate
    {
        [Key]
        public Guid AgencyId { get; set; }

        public int? RequestSource { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NextRefreshDate { get; set; }
    }
}
