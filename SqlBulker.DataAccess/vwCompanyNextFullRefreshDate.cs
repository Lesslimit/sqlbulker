namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwCompanyNextFullRefreshDate
    {
        [Key]
        public Guid CompanyId { get; set; }

        public int? FullRefreshFrequency { get; set; }

        public DateTime? PreviousFullRefreshDate { get; set; }

        public DateTime? NextFullRefreshDate { get; set; }
    }
}
