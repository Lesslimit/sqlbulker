namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwCompanyNextPartialRefreshDate
    {
        [Key]
        public Guid CompanyId { get; set; }

        public Guid? AgencyId { get; set; }

        public int? PartialRefreshFrequency { get; set; }

        public DateTime? PreviousRefreshDate { get; set; }

        public DateTime? NextPartialRefreshDate { get; set; }
    }
}
