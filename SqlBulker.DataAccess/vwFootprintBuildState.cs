namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwFootprintBuildState")]
    public partial class vwFootprintBuildState
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        public Guid? CompanyId { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        public Guid? AgencyId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DateCreated { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RequestSource { get; set; }

        [StringLength(24)]
        public string RequestSourceExt { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BuildType { get; set; }

        [StringLength(30)]
        public string BuildTypeExt { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IsFailed { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IsSuccess { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IsPending { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DataDiscoveryState { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ScoringState { get; set; }

        [Key]
        [Column(Order = 9)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MetricsState { get; set; }

        public TimeSpan? Duration { get; set; }

        public int? DurationSec { get; set; }

        public Guid? SetId { get; set; }
    }
}
