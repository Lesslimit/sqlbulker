namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwListView")]
    public partial class vwListView
    {
        [Key]
        [Column(Order = 0)]
        public Guid CompanyId { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(250)]
        public string LocationName { get; set; }

        [StringLength(50)]
        public string W2CompanyId { get; set; }

        [StringLength(50)]
        public string StoreId { get; set; }

        public Guid? AgencyId { get; set; }

        [StringLength(250)]
        public string Website { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompetitorCount { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string CompleteAddress { get; set; }

        public int? BrandifyScore { get; set; }

        public int? BrandScoreStrength { get; set; }

        public int? BrandifyScoreDelta { get; set; }

        public int? DataQualityScore { get; set; }

        public int? DataQualityScoreStrength { get; set; }

        public int? DataQualityScoreDelta { get; set; }

        public double? DataQualityScorePercentage { get; set; }

        public int? SocialScore { get; set; }

        public int? SocialScoreStrength { get; set; }

        public int? EngagementScoreDelta { get; set; }

        public double? EngagementScorePercentage { get; set; }

        public int? ReviewScore { get; set; }

        public int? ReviewsScoreStrength { get; set; }

        public int? ReviewsScoreDelta { get; set; }

        public double? ReviewsScorePercentage { get; set; }

        public int? SearchScore { get; set; }

        public int? SearchScoreStrength { get; set; }

        public int? SearchScoreDelta { get; set; }

        public double? SearchScorePercentage { get; set; }

        public bool? BingClaimed { get; set; }

        public long? BingReviews { get; set; }

        public long? BingReviewsDelta { get; set; }

        public double? BingRating { get; set; }

        public double? BingRatingDelta { get; set; }

        public long? FacebookCheckIns { get; set; }

        public long? FacebookCheckInsDelta { get; set; }

        public bool? IsFacebookClaimed { get; set; }

        public long? FacebookComments { get; set; }

        public long? FacebookCommentsDelta { get; set; }

        public long? FacebookLikes { get; set; }

        public long? FacebookLikesDelta { get; set; }

        public long? FacebookPosts { get; set; }

        public long? FacebookPostsDelta { get; set; }

        public long? FacebookTalkingAbout { get; set; }

        public long? FacebookTalkingAboutDelta { get; set; }

        public long? FoursquareCheckins { get; set; }

        public long? FoursquareCheckinsDelta { get; set; }

        public bool? IsFoursquareClaimed { get; set; }

        public double? FoursquareRating { get; set; }

        public double? FoursquareRatingDelta { get; set; }

        public long? FoursquareTips { get; set; }

        public long? FoursquareTipsDelta { get; set; }

        public long? FoursquareUsers { get; set; }

        public long? FoursquareUsersDelta { get; set; }

        public long? GooglePlusActivities { get; set; }

        public long? GooglePlusActivitiesDelta { get; set; }

        public long? GooglePlusCircledCount { get; set; }

        public long? GooglePlusCircledByDelta { get; set; }

        public long? GooglePlusOneCount { get; set; }

        public long? GooglePlusOneDelta { get; set; }

        public double? GoogleReviewRating { get; set; }

        public double? GoogleReviewRatingDelta { get; set; }

        public long? LinkedInFollowers { get; set; }

        public long? LinkedInFollowersDelta { get; set; }

        public long? PinterestFollowers { get; set; }

        public long? PinterestFollowersDelta { get; set; }

        public long? PinterestPins { get; set; }

        public long? PinterestPinsDelta { get; set; }

        public long? SEMRushAdsTraffic { get; set; }

        public long? SEMRushAdsTrafficDelta { get; set; }

        public long? SEMRushSearchTraffic { get; set; }

        public long? SEMRushSearchTrafficDelta { get; set; }

        public long? TripAdvisorAreaRank { get; set; }

        public long? TripAdvisorAreaRankDelta { get; set; }

        public double? TripAdvisorRating { get; set; }

        public double? TripAdvisorRatingDelta { get; set; }

        public long? TripAdvisorReviews { get; set; }

        public long? TripAdvisorReviewsDelta { get; set; }

        public long? TwitterFollowers { get; set; }

        public long? TwitterFollowerDelta { get; set; }

        public long? TwitterFriends { get; set; }

        public long? TwitterFriendsDelta { get; set; }

        public long? TwitterListed { get; set; }

        public long? TwitterListedDelta { get; set; }

        public long? TwitterMentions { get; set; }

        public long? TwitterMentionsDelta { get; set; }

        public long? TwitterStatus { get; set; }

        public long? TwitterStatusDelta { get; set; }

        public double? YahooAverageRating { get; set; }

        public double? YahooAverageRatingDelta { get; set; }

        public long? YahooTotalRatings { get; set; }

        public long? YahooTotalRatingsDelta { get; set; }

        public long? YahooTotalReviews { get; set; }

        public long? YahooTotalReviewsDelta { get; set; }

        public bool? IsYelpClaimed { get; set; }

        public double? YelpRating { get; set; }

        public double? YelpRatingDelta { get; set; }

        public long? YelpReviews { get; set; }

        public long? YelpReviewsDelta { get; set; }

        public long? YouTubeFavorites { get; set; }

        public long? YouTubeFavoritesDelta { get; set; }

        public long? YouTubeChannelSubscribers { get; set; }

        public long? YouTubeChannelSubscribersDelta { get; set; }

        public long? YouTubeVideoViews { get; set; }

        public long? YouTubeVideoViewsDelta { get; set; }

        public double? YellowPagesRating { get; set; }

        public double? MerchantCircleRating { get; set; }

        public DateTime? LastRefreshDate { get; set; }
    }
}
