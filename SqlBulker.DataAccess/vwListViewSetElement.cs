namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwListViewSetElement
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string SetId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ElementId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string FieldName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public string FullDescription { get; set; }

        [StringLength(50)]
        public string CssClass { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderGroup { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DisplayOrder { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(50)]
        public string GroupName { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool IsDefault { get; set; }

        [Key]
        [Column(Order = 7)]
        public bool IsEditable { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ComparerType { get; set; }

        [StringLength(5)]
        public string ValueSuffix { get; set; }
    }
}
