namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwScoreStrengthRange")]
    public partial class vwScoreStrengthRange
    {
        [Key]
        [Column(Order = 0)]
        public Guid RuleSetId { get; set; }

        [StringLength(255)]
        public string RuleSet { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string Group { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(20)]
        public string Strength { get; set; }

        [Key]
        [Column(Order = 4)]
        public double LowerBound { get; set; }

        [Key]
        [Column(Order = 5)]
        public double UpperBound { get; set; }
    }
}
