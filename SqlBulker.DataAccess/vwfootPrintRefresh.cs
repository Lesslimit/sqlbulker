namespace SqlBulker.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vwfootPrintRefresh
    {
        [Key]
        public Guid AgencyId { get; set; }

        [StringLength(255)]
        public string AgencyName { get; set; }

        public int? LocationCount { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(50)]
        public string AgencyPartner { get; set; }

        public Guid? AgencyPartnerId { get; set; }

        public DateTime? LastAgencyRefreshDate { get; set; }

        public Guid? CompanyId { get; set; }

        public DateTime? CompanyRefreshDate { get; set; }

        public int? RequestSource { get; set; }

        [StringLength(24)]
        public string RequestSourceExt { get; set; }

        public int? BuildType { get; set; }

        [StringLength(30)]
        public string BuildTypeExt { get; set; }

        public int? IsFailed { get; set; }

        public int? IsSuccess { get; set; }

        public int? IsPending { get; set; }

        public TimeSpan? Duration { get; set; }
    }
}
