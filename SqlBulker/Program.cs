﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using LoremNET;
using SqlBulker.Core.Extensions;
using SqlBulker.DataAccess;

namespace SqlBulker
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var random = new Random();
                using (var context = new BrandifyEntities())
                {
                    var companyReviews = new List<CompanyReview>(11);

                    for (int i = 0; i < 11; i++)
                    {
                        companyReviews.Add(new CompanyReview
                        {
                            SolrId = Guid.NewGuid(),
                            CompanyId = Guid.Parse("41269F62-CF32-4CDD-A627-0008295AD573"),
                            DateCreated = DateTime.UtcNow,
                            ReviewDate = Lorem.DateTime(2010),
                            ProviderId = random.Next(-2, 14),
                            Rating = random.NextDouble(),
                            Title = Lorem.Words(1, 3),
                            Reviewer = Lorem.Words(1),
                            URL = string.Format("https://{0}.com", Lorem.Words(1).ToLower()),
                            ProfileId = random.Next(1, 5000).ToString(CultureInfo.InvariantCulture),
                            CompanyReviewText = new CompanyReviewText { Text = Lorem.Sentence(5, 20) },
                            CompanyReviewLogs =
                            {
                                new CompanyReviewLog
                                {
                                    Action = ActionType.Created,
                                    DateCreated = DateTime.UtcNow,
                                    Description = "Migrated from Solr " + i
                                },
                                new CompanyReviewLog
                                {
                                    Action =  ActionType.Edited,
                                    DateCreated = DateTime.UtcNow,
                                    Description = "Migrated from Solr " + i + 1
                                },
                                new CompanyReviewLog
                                {
                                    Action = ActionType.Posted,
                                    DateCreated = DateTime.UtcNow,
                                    Description = "Migrated from Solr " + i + 2
                                }
                            }
                        });
                    }

                    context.CompanyReviews.AddRange(companyReviews);

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    context.SaveChangesBulk();

                    Console.WriteLine(stopwatch.ElapsedMilliseconds);

                    Console.ReadKey();
                }
            }
            catch (Exception exception)
            {
                var dbEntityValidationException = exception as DbEntityValidationException;
                throw;
            }
        }
    }
}
